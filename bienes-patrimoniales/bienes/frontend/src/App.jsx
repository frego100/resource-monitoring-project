import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';

import Login from './componentes/Login';
import NuevaCuenta from './componentes/NuevaCuenta';
import Dashboard from './componentes/Dashboard/Dashboard';

import './App.css';
import { BrowserRouter as Router, Switch, Route  } from 'react-router-dom';
import GestionEntidad from './componentes/Dashboard/Dashboard';
import GestionAreas from './componentes/Dashboard/Dashboard';
import GestionOficinas from './componentes/Dashboard/Dashboard';

function App() {
  return (
    <Router>
      <Switch>

        <Route exact path="/" component={Login} />
        <Route exact path="/nueva-cuenta" component={NuevaCuenta} />
        <Route exact path="/dashboard" component={Dashboard} />
        <Route exact path="/gestion-entidad" component={GestionEntidad} />
        <Route exact path="/gestion-areas" component={GestionAreas} />
        <Route exact path="/gestion-oficinas" component={GestionOficinas} />
      </Switch>
      
    </Router>
  );
}

export default App;
