import React from 'react';
import {CardDeck, Card, Image, Button } from 'react-bootstrap';
import "./Login.css";

const Login = () => {
    const _handleSignInClick = () => {
        // Authenticate using via passport api in the backend
        // Open Twitter login page
        // Upon successful login, a cookie session will be stored in the client
        window.open("http://localhost:4000/auth/google", "_self");
      };
    return (
        <div className="wrapper">
            <CardDeck>
                <Card style={{ borderRadius: 15}}>
                    <Card.Body>
                        <Card.Title as="h3"><em>Oficina de Control Patrimonial</em></Card.Title>
                        <Card.Img className="img-fluid" src="/libros-unsa.png" 
                                style={{width:400}}></Card.Img>
                    </Card.Body>    
                </Card>
                <Card style={{ width: '16rem' , height:'22rem', borderRadius: 15}}>
                    <Card.Body>
                        <Image  className="img-fluid" style={{width:230, height:150, padding: 15}}
                                src="/logo_unsa.png" rounded/>
                        <Card.Text>
                            {' '}Ingrese con su correo institucional.{' '}
                        </Card.Text>
                        <Button onClick={_handleSignInClick} variant="dark" block>Iniciar Sesión</Button>
                        <p>*Si desea registrarse, contáctese con la Oficina de Patrimonio*</p>
                    </Card.Body>
                </Card>
            </CardDeck>
        </div>
    )
}

export default Login
