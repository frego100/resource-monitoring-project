import React from 'react'
import ModalEliminarAreas from './ModalEliminarAreas'
import ModalEditarAreas from './ModalEditarAreas'


function RowArea({area, listarAreas}) {
    
    return (
        <>
            <tr>
                <th>{area._id}</th>
                <th> {area.siglas_area}</th>
                <th>{area.nombre_area}</th>
                <th className="text-center">
                    <ModalEliminarAreas id={area._id} listarAreas={listarAreas} /> 
                    <ModalEditarAreas id={area._id} listarAreas={listarAreas} /> 
                </th>
            </tr>
        </>
    );
}

export default RowArea