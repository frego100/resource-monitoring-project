import React from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import { useEffect } from 'react';

function ModalCrearAreas({ listarAreas }) {

    const [show, setShow] = React.useState(false);

    const [local, setLocales] = React.useState([]);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [area, setArea] = React.useState({});

    const [validated, setValidated] = React.useState(false);

    const handleSubmit = (event) => {
        console.log('asd')
        const form = event.currentTarget;
        event.preventDefault();
        if (form.checkValidity() === false) {
            event.stopPropagation();
        } else {
            crearArea();
            handleClose();
        }

        setValidated(true);
    };


    const crearArea = () => {

        const apiUrl = 'http://localhost:4000/api/areas';


        console.log(area)
        fetch(apiUrl, { method: 'POST', body: JSON.stringify(area), headers: { 'Content-type': 'application/json' } })
            .then((response) => response.json())
            .then((data) => {
                console.log('This is your data', data);
                listarAreas();
                console.log(listarAreas);
            });

    }

    useEffect(() => {
        listarLocales();
    }, [])

    const listarLocales = () => {
        const apiUrl = 'http://localhost:4000/api/locales';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setLocales(data));
    }

    const actualizarState = (e) => {
        setArea({
            ...area,
            [e.target.name]: e.target.value
        })
    }

    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Crear Area
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                        <Modal.Title>Crear Area</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="container">
                            <div className="row py-3">
                                <div className="col-8 offset-2">

                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Id Area</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Id de Area" name="_id" onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Selecionar Local al que pertenece</Form.Label>
                                        <Form.Control required as="select" name="id_local" onChange={actualizarState}>
                                            {
                                                local.map(data => (
                                                    <option value={data._id}>{data.nombre}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Nombre</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese nombre" name="nombre_area" onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlInput4">
                                        <Form.Label>Sigla</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese sigla" name="siglas_area" onChange={actualizarState} />
                                    </Form.Group>


                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button type="submit" className="color-guinda" variant="secondary">
                            Crear
                    </Button>
                        <Button className="color-cancelar" variant="primary" onClick={handleClose}>
                            Cancelar
                    </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}

export default ModalCrearAreas