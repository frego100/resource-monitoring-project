import React, { useState, useEffect } from 'react'
import RowArea from './RowArea'
import ModalCrearAreas from './ModalCrearAreas'

import { Form, Table, Pagination } from 'react-bootstrap'

const GestionAreas = () => {

    const [areas, setAreas] = useState([]);

    useEffect(() => {

        listarAreas();

    }, []);

    const listarAreas = () => {
        const apiUrl = 'http://localhost:4000/api/areas';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setAreas(data));
    };

    return (
        <>
            <div className="container">

                <div className="row pt-5 pb-1">
                    <div className="col-10">
                        <h3>Gestion Areas</h3>
                    </div>
                </div>

                <div className="row py-4">
                    <div className="col-4">
                        <Form>
                            <Form.Group controlId="formBasicEmail" className="m-0">
                                <Form.Control type="email" placeholder="Buscar Area" />
                            </Form.Group>
                        </Form>
                    </div>

                    <div className="col-4 offset-4 text-right">
                        <ModalCrearAreas  listarAreas={listarAreas}/>

                    </div>
                </div>


                <div className="row py-4">
                    <div className="col">
                        <Table responsive className="tabla-color">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Siglas</th>
                                    <th>Nombre</th>
                                    <th className="text-center">Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                {areas.map(data => (

                                    <RowArea
                                        area={data}
                                        key={data.id}
                                        listarAreas={listarAreas}
                                    />
                                ))}
                            </tbody>
                        </Table>
                    </div>
                </div>

                <div className="col-12 my-4">
                    <div className="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">
                        <div className="btn-group mr-2" role="group" aria-label="First group">
                            <Pagination>
                                <Pagination.Item active>{1}</Pagination.Item>
                                <Pagination.Item>{2}</Pagination.Item>
                                <Pagination.Item >{3}</Pagination.Item>
                                <Pagination.Item>{4}</Pagination.Item>
                                <Pagination.Item >{5}</Pagination.Item>
                            </Pagination>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default GestionAreas
