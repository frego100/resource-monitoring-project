import React from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import { useEffect } from 'react';

function ModalEditarAreas({id, listarAreas}) {

    const [show, setShow] = React.useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [area, setArea] = React.useState({});
    const [local, setLocales] = React.useState([]);

    useEffect (()=>{
        apiArea();
        listarLocales();
    },[])

    const listarLocales = () =>{
        const apiUrl = 'http://localhost:4000/api/locales';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setLocales(data));
    }
    const apiArea=()=>{
        const apiUrl = 'http://localhost:4000/api/areas/'+id;
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setArea(data));
    }

    const actualizarArea = () => {

        const apiUrl = 'http://localhost:4000/api/areas/'+id;
        console.log(area)
        fetch(apiUrl, {method: 'PUT', body: JSON.stringify(area),headers:{'Content-type':'application/json'}   })
            .then((response) => response.json())
            .then((data) => {
                console.log('This is your data', data);
                listarAreas();
                console.log(listarAreas);
        });

    }

    const actualizarState = (e) => {
        setArea({
            ...area,
            [e.target.name]: e.target.value
        })
    }


    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Editar Area
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Editar Area</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div className="container">
                        <div className="row py-3">
                            <div className="col-8 offset-2">
                                <Form>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Id Area</Form.Label>
                                        <Form.Control type="email" placeholder="Ingrese Id de Area" value={area._id} readOnly/>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Selecionar Local al que pertenece</Form.Label>
                                        <Form.Control as="select" name="id_local" defaultValue={area.id_local} onChange={actualizarState}>
                                            {
                                                local.map( data => (
                                                    <option value={data._id}>{data.nombre}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Nombre</Form.Label>
                                        <Form.Control type="email" placeholder="Ingrese nombre" name="nombre_area" defaultValue={area.nombre_area} onChange={actualizarState}/>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Sigla</Form.Label>
                                        <Form.Control type="email" placeholder="Ingrese sigla" name="siglas_area" onChange={actualizarState} defaultValue={area.siglas_area}/>
                                    </Form.Group>                                                                
                                    
                                
                                </Form>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="color-guinda"  variant="secondary" onClick={()=>{
                        handleClose();
                        actualizarArea();}}>
                        Editar
                    </Button>
                    <Button className="color-cancelar" variant="primary" onClick={handleClose}>
                        Cancelar
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default ModalEditarAreas