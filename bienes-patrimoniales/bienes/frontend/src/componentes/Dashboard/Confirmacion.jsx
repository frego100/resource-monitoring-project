import React from 'react'
import { Button, Modal } from 'react-bootstrap'

function Confirmacion({ usuario, authenticated }) {
    const [show, setShow] = React.useState(true);

    const handleClose = () => setShow(false);
    //const handleShow = () => setShow(true);
    const _handleSignInClick = () => {
        // Authenticate using via passport api in the backend
        // Open Twitter login page
        // Upon successful login, a cookie session will be stored in the client
        window.open("http://localhost:4000/auth/google", "_self");
    };
    const _handleLogoutClick = () => {
        // Set authenticated state to false in the HomePage
        window.open("http://localhost:4000/auth/logout", "_self");
    };

    return (
        <>
            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                {authenticated ? (
                    <>
                    <Modal.Header >
                        <Modal.Title>Bienvenido {usuario.nombre}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        Usted se ha ingresado al sistema con el usuario: .
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="primary" onClick={handleClose}>
                            Entendido
                        </Button>
                        <Button variant="secondary" onClick={_handleSignInClick}>
                            ¿No es Usted?
                        </Button>
                    </Modal.Footer></>) : (<>
                        <Modal.Header >
                            <Modal.Title>Verificacion de Identidad Requerida</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Usted debe de registrarse y autenticarse con una cuenta valida, para tener acceso al sistema.
                </Modal.Body>
                        <Modal.Footer>
                            <Button variant="primary" onClick={_handleLogoutClick}>
                                Salir
                    </Button>
                            <Button variant="secondary" onClick={_handleSignInClick}>
                                Identifacarse
                    </Button>
                        </Modal.Footer></>)}


            </Modal>
        </>
    );
}

export default Confirmacion