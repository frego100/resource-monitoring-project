import React from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import { useEffect } from 'react';

function ModalEditarEntidad({id, listarEntidades}) {
    const [show, setShow] = React.useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [entidad, setEntidad] = React.useState({});

    useEffect (()=>{
        apiEntidad();
    },[])

    const apiEntidad=()=>{
        const apiUrl = 'http://localhost:4000/api/entidades/'+id;
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setEntidad(data));
    }

    const actualizarEntidad = () => {

        const apiUrl = 'http://localhost:4000/api/entidades/'+id;
        console.log(entidad)
        fetch(apiUrl, {method: 'PUT', body: JSON.stringify(entidad),headers:{'Content-type':'application/json'}   })
            .then((response) => response.json())
            .then((data) => {
                console.log('This is your data', data);
                listarEntidades();
                console.log(listarEntidades);
        });

    }
    const actualizarState = (e) => {
        setEntidad({
            ...entidad,
            [e.target.name]: e.target.value
        })
    }

    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Editar Entidad
            </Button>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
                <Modal.Header closeButton>
                        <Modal.Title>Editar Entidad</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <div className="container">
                        <div className="row py-3">
                            <div className="col-8 offset-2">
                                <Form>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Código</Form.Label>
                                        <Form.Control type="text" value={entidad._id} readOnly/>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlInput2">
                                        <Form.Label>Nombre</Form.Label>
                                        <Form.Control type="text" placeholder="Nombre" name="nombre_entidad" defaultValue={entidad.nombre_entidad} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Dependencia</Form.Label>
                                        <Form.Control type="text" placeholder="Dependencia" name="dependencia" defaultValue={entidad.dependencia} onChange={actualizarState} />
                                    </Form.Group>
                                </Form>
                            </div>
                        </div>
                    </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button className="color-guinda"  variant="secondary" onClick={()=>{
                            handleClose();
                            actualizarEntidad();}}>
                            Editar
                        </Button>
                        <Button className="color-cancelar" variant="primary" onClick={handleClose}>
                            Cancelar
                        </Button>
                    </Modal.Footer>
            </Modal>
        </>
    );
}
export default ModalEditarEntidad