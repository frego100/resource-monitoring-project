import React from 'react'
import ModalEliminarEntidad from './ModalEliminarEntidad.jsx'
import ModalEditarEntidad from './ModalEditarEntidad.jsx'

function RowEntidad({entidad, listarEntidades}) {
    
    return (
        <>
            <tr>
                <th>{entidad._id}</th>
                <th>{entidad.nombre_entidad}</th>
                <th>{entidad.dependencia}</th>
                <th className="text-center">
                    <ModalEliminarEntidad id={entidad._id} listarEntidades={listarEntidades} /> 
                    <ModalEditarEntidad id={entidad._id} listarEntidades={listarEntidades} /> 
                </th>
            </tr>
        </>
    );
}

export default RowEntidad