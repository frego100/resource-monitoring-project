import React, {useState, useEffect} from 'react'
import RowEntidad from './RowEntidad'
import ModalCrearEntidad from './ModalCrearEntidad'
import {Form,Table, Pagination } from 'react-bootstrap'

const GestionEntidad = () => {

    const [entidades, setEntidades] = useState([]);

    useEffect(() => {

        listarEntidades();

    }, []);

    const listarEntidades = () => {
        const apiUrl = 'http://localhost:4000/api/entidades';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setEntidades(data));
    };

    return (
        <>
        <div className="container">
            <div className="row pt-5 pb-1">
                <div className="col-10">
                    <h3>Gestión de Entidades</h3>
                </div>
            </div>
            <div className="row py-4">
                <div className="col-4">
                    <Form>
                        <Form.Group controlId="formBasicEmail" className="m-0">
                            <Form.Control type="email" placeholder="Buscar Entidad" />
                            </Form.Group>
                    </Form>
                </div>     
                <div className="col-4 offset-4 text-right">
                    <ModalCrearEntidad listarEntidades = {listarEntidades}/>
                </div>     
            </div>
            <div className="row py-4">
                <div className="col">
                <Table responsive className="tabla-color">
                    <thead>
                        <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Dependencia</th>
                        <th className="text-center">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        {entidades.map( data => (
                                    
                            <RowEntidad  
                                entidad={data} 
                                key={data.id}
                                listarEntidades={listarEntidades}
                            />
                        ))}
                    </tbody>
                </Table>
                </div>
            </div>
            <div className="col-12 my-4">
                    <div className="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">
                        <div className="btn-group mr-2" role="group" aria-label="First group">
                            <Pagination>
                                <Pagination.Item active>{1}</Pagination.Item>
                                <Pagination.Item>{2}</Pagination.Item>
                                <Pagination.Item >{3}</Pagination.Item>
                                <Pagination.Item>{4}</Pagination.Item>
                                <Pagination.Item >{5}</Pagination.Item>
                            </Pagination>
                        </div>
                    </div>
            </div>
        </div>
        </>
    );
}
export default GestionEntidad

