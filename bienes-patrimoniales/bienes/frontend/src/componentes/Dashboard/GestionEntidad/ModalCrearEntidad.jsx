import React from 'react'
import { Button, Modal, Form } from 'react-bootstrap'

function ModalCrearEntidad({ listarEntidades }) {
    const [show, setShow] = React.useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [entidad, setEntidad] = React.useState({});

    const [validated, setValidated] = React.useState(false);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        if (form.checkValidity() === false) {
            event.stopPropagation();
        } else {
            crearEntidad();
            handleClose();
        }

        setValidated(true);
    };


    const crearEntidad = () => {
        const apiUrl = "http://localhost:4000/api/entidades";
        console.log('enviaras:')
        console.log(entidad);
        fetch(apiUrl, { method: 'POST', body: JSON.stringify(entidad), headers: { 'Content-type': 'application/json' } })
            .then((response) => response.json())
            .then((data) => {
                console.log('This is your data' + data);
                listarEntidades();

            })
    }

    const actualizarState = (e) => {
        setEntidad({
            ...entidad,
            [e.target.name]: e.target.value
        })
    }

    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Crear Entidad
        </Button>

            <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                        <Modal.Title>Crear Entidad</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div className="container">
                            <div className="row py-3">
                                <div className="col-8 offset-2">

                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Código</Form.Label>
                                        <Form.Control required type="text" placeholder="Código" name="_id" onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlInput2">
                                        <Form.Label>Nombre</Form.Label>
                                        <Form.Control required type="text" placeholder="Nombre" name="nombre_entidad" onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Dependencia</Form.Label>
                                        <Form.Control required type="text" placeholder="Dependencia" name="dependencia" onChange={actualizarState} />
                                    </Form.Group>

                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button type="submit" className="color-guinda" variant="secondary">Crear</Button>
                        <Button className="color-cancelar" variant="primary" onClick={handleClose}>Cancelar</Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}

export default ModalCrearEntidad