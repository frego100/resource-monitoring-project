import React from 'react'

import { Button, Modal, Form } from 'react-bootstrap'

function ModalEntidad() {
    const [show, setShow] = React.useState(false);
  
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
  
    return (
      <>
        <Button className="color-secundario" variant="primary" onClick={handleShow}>Crear Entidad</Button>
  
        <Modal show={show} onHide={handleClose} backdrop="static" keyboard={false}>
            <Modal.Header closeButton>
                <Modal.Title>Crear Entidad</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <div className="container">       
                <div className="row">
                    <div className="col">
                        <Form>  
                            <Form.Group className="form-group row">
                                <Form.Label className="col-sm-3 col-form-label" >Código</Form.Label>
                                <Form.Control className="col-sm-8" type="text" placeholder="Código" id="id_entidad"/>
                            </Form.Group>
                            <Form.Group className="form-group row">
                                <Form.Label className="col-sm-3 col-form-label">Nombre</Form.Label>
                                <Form.Control className="col-sm-8" type="text" placeholder="Nombre" id="nombre_entidad"/>
                            </Form.Group>
                            <Form.Group className="form-group row">
                                <Form.Label  className="col-sm-3 col-form-label">Dependencia</Form.Label>
                                <Form.Control className="col-sm-8" type="text" placeholder="Dependencia" id="dependencia"/>
                            </Form.Group>
                        </Form>
                    </div>
                </div>
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button className="color-guinda" type="submit" variant="secondary" onClick={handleClose}>Crear</Button>
                <Button className="color-cancelar" variant="primary" onClick={handleClose}>Cancelar</Button>
            </Modal.Footer>
        </Modal>
      </>
    );
}

export default ModalEntidad