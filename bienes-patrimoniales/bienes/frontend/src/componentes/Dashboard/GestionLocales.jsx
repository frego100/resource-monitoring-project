import React from 'react'

const GestionLocales = () => {
    return (

        <table class="table">
        <tr>
          <th>Id</th>
          <th>Nombre</th>
          <th>Acciones</th>
        </tr>
        
        <tr>
          <td>1</td>
          <td>Local1</td>
          <td><a href="https://www.Editar_Entidad.com">editar</a> /  <a href="https://www.Eliminar_Entidad.com"> eliminar </a></td>
        </tr>
        
        <tr>
          <td>2</td>
          <td>Local2</td>
          <td><a href="https://www.Editar_Entidad.com">editar</a> /  <a href="https://www.Eliminar_Entidad.com"> eliminar </a></td>
        </tr>
        
        <tr>
          <td>3</td>
          <td>Local3</td>
          <td><a href="https://www.Editar_Entidad.com">editar</a> /  <a href="https://www.Eliminar_Entidad.com"> eliminar </a></td>
        </tr>
        </table>
    )
}

export default GestionLocales
