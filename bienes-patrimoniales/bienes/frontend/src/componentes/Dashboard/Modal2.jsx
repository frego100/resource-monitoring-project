import React from 'react';
import ReactDOM from 'react-dom';

const Modal = ({ isShowing, hide }) => isShowing ? ReactDOM.createPortal(
    <React.Fragment>
        {/* <div className="modal-overlay" /> */}
        <div className="modal d-block" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title" id="exampleModalLabel">Crear Area2</h5>
                        <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={hide}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div className="modal-body">
                     <div className="container">
                        
                        <div className=" row ">
                             <div className="col ">
                        <form>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Local</label>
                                <div className="col-sm-5">
                                    <select className="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                        <option selected>Choose</option>
                                        <option value="1">A</option>
                                        <option value="2">B</option>
                                        <option value="3">C</option>
                                    </select>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Codigo</label>
                                <div className="col-sm-5">
                                    <select className="custom-select mr-sm-2" id="inlineFormCustomSelect">
                                        <option selected>Choose</option>
                                        <option value="1"></option>
                                        <option value="2">B</option>
                                        <option value="3">C</option>
                                    </select>
                                </div>
                                    
                            </div>

                            <div className="form-group row ">
                                <label className="col-sm-2 col-form-label">Nombre</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Ingresar nombre de Area"/>
                                </div>
                            </div>

                            <div className="form-group row">
                                <label className="col-sm-2 col-form-label">Sigla</label>
                                <div className="col-sm-10">
                                    <input type="text" className="form-control" id="formGroupExampleInput2" placeholder="Ingresar SIGLA"/>
                                </div>
                            </div>
                            

                                        
                        
                        </form>
                    </div>
                </div>
            </div>


                        
      </div>
                    <div className="modal-footer">
                        <button type="button" className="btn btn-primary">Crear</button>
                        <button type="button" className="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                        
                    </div>
                </div>
            </div>
        </div>
    </React.Fragment>, document.body
) : null;

export default Modal;