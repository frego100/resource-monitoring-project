import React from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import { useEffect } from 'react';

function ModalEditarOficinas({id, listarOficinas}) {

    const [show, setShow] = React.useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [oficina, setOficina] = React.useState([]);
    const [area, setAreas] = React.useState([]);

    useEffect (()=>{
        apiOficina();
        listarAreas();
    },[])

    const listarAreas = () => {
        const apiUrl = 'http://localhost:4000/api/areas';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setAreas(data));
    };

    const apiOficina=()=>{
        const apiUrl = 'http://localhost:4000/api/oficinas/'+id;
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setOficina(data));
    }
    
    const actualizarOficina = () => {

        const apiUrl = 'http://localhost:4000/api/oficinas/'+id;
        console.log(oficina)
        fetch(apiUrl, {method: 'PUT', body: JSON.stringify(oficina),headers:{'Content-type':'application/json'}   })
            .then((response) => response.json())
            .then((data) => {
                console.log('This is your data', data);
                listarOficinas();
                console.log(listarOficinas);
        });

    }

    const actualizarState = (e) =>{
        setOficina({
            ...oficina,
            [e.target.name] : e.target.value
        })
    }

    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Editar Oficina
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Editar Oficina</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <div className="row py-3">
                            <div className="col-8 offset-2">
                                <Form>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Selecionar Area a la que pertenece</Form.Label>
                                        <Form.Control as="select" name="id_area" defaultValue={oficina.id_area} onChange={actualizarState}>
                                            {
                                                area.map( data => (
                                                    <option value={data._id}>{data.nombre_area}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlInput1">
                                        <Form.Label>Nombre de Oficina</Form.Label>
                                        <Form.Control type="email" placeholder="Ingrese nombre" name="nombre_oficina" defaultValue={oficina.nombre_oficina} onChange={actualizarState}/>
                                    </Form.Group>                                                            
                                </Form>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                <Button className="color-guinda"  variant="secondary" onClick={()=>{
                        handleClose();
                        actualizarOficina();}}>
                        Editar
                    </Button>
                    <Button className="color-cancelar" variant="primary" onClick={handleClose}>
                        Cancelar
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}
export default ModalEditarOficinas