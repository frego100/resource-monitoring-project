import React, { useState, useEffect } from 'react'
import RowOficinas from './RowOficinas'
import ModalCrearOficinas from './ModalCrearOficinas'

import { Form, Table, Pagination } from 'react-bootstrap'


const GestionOficinas = () => {

    const [oficinas, setOficinas] = useState([]);
    useEffect(() => {
        listarOficinas();
    }, []);

    const listarOficinas = () => {
        const apiUrl = 'http://localhost:4000/api/oficinas';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setOficinas(data))
    };


    return (
        <>
            <div className="container">
                <div className="row pt-5 pb-1">
                    <div className="col-10">
                        <h3>Gestion Oficinas</h3>
                    </div>
                </div>

                <div className="row py-4">
                    <div className="col-4">
                        <Form>
                            <Form.Group controlId="formBasicEmail" className="m-0">
                                <Form.Control type="email" placeholder="Buscar Oficina" />
                            </Form.Group>
                        </Form>
                    </div>

                    <div className="col-4 offset-4 text-right">
                        <ModalCrearOficinas listarOficinas={listarOficinas} />

                    </div>
                </div>


                <div className="row py-4 ">
                    <div className="col">
                        <Table responsive className="tabla-color">
                            <thead>
                                <tr>
                                    <th>Nombre de la Oficina</th>
                                </tr>
                            </thead>
                            <tbody>
                                {oficinas.map(data => (

                                    <RowOficinas
                                        oficinas={data}
                                        key={data.id}
                                        listarOficinas={listarOficinas}
                                    />
                                ))}
                            </tbody>
                        </Table>
                    </div>
                </div>

                <div className="col-12 my-4">
                    <div className="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">
                        <div className="btn-group mr-2" role="group" aria-label="First group">
                            <Pagination>
                                <Pagination.Item active>{1}</Pagination.Item>
                                <Pagination.Item>{2}</Pagination.Item>
                                <Pagination.Item >{3}</Pagination.Item>
                                <Pagination.Item>{4}</Pagination.Item>
                                <Pagination.Item >{5}</Pagination.Item>
                            </Pagination>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default GestionOficinas
