import React from 'react'
import ModalEliminarOficinas from './ModalEliminarOficinas'
import ModalEditarOficinas from './ModalEditarOficinas'

function RowOficinas({oficinas, listarOficinas}) {
    
    return (
        <>
            <tr>
                <th>{oficinas.nombre_oficina}</th>
                <th className="text-center">
                    <ModalEliminarOficinas id={oficinas._id} listarOficinas={listarOficinas} /> 
                    <ModalEditarOficinas id={oficinas._id} listarOficinas={listarOficinas} /> 
                </th>
            </tr>
        </>
    );
}

export default RowOficinas