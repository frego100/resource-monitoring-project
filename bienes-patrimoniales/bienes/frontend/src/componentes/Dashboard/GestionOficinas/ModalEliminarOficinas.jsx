import React from 'react'
import { Button, Modal } from 'react-bootstrap'

function ModalEliminarOficinas({id, listarOficinas}) {

    const [show, setShow] = React.useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const eliminarOficina = () =>{
        const apiUrl = 'http://localhost:4000/api/oficinas/'+id;

        fetch(apiUrl, {method: 'DELETE'})
            .then((response)=> response.json())
            .then((data) =>{
                console.log('This is your data', data);
                listarOficinas();
            })
    }

    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Eliminar
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Eliminar Oficinas</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <div className="row py-3">
                            <div className="col-12">
                                <h2>¿Desea eliminar el objeto?</h2>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="color-guinda" variant="secondary" onClick={()=>
                        {eliminarOficina(); handleClose();}}>
                        Eliminar
                    </Button>
                    <Button className="color-cancelar" variant="primary" onClick={handleClose}>
                        Cancelar
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default ModalEliminarOficinas