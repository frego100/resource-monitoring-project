import React from 'react'
import { Button, Modal, Form } from 'react-bootstrap'
import { useEffect } from 'react';

function ModalCrearOficinas({ listarOficinas }) {

    const [show, setShow] = React.useState(false);

    const [area, setAreas] = React.useState([]);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [oficina, setOficina] = React.useState([]);

    const [validated, setValidated] = React.useState(false);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        if (form.checkValidity() === false) {
            event.stopPropagation();
        } else {
            crearOficina();
            handleClose();
        }

        setValidated(true);
    };

    const crearOficina = () => {
        const apiUrl = "http://localhost:4000/api/oficinas";
        console.log(oficina)
        fetch(apiUrl, { method: 'POST', body: JSON.stringify(oficina), headers: { 'Content-type': 'application/json' } })
            .then((response) => response.json())
            .then((data) => {
                console.log('This is your data', data);
                listarOficinas();
                console.log(listarOficinas);
            })
    }

    useEffect(() => {
        listarAreas();
    }, [])


    const listarAreas = () => {
        const apiUrl = 'http://localhost:4000/api/areas';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setAreas(data));
    }

    const actualizarState = (e) => {
        setOficina({
            ...oficina,
            [e.target.name]: e.target.value
        })
    }

    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Crear Oficina
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                        <Modal.Title>Crear Oficina</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="container">
                            <div className="row py-3">
                                <div className="col-8 offset-2">

                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Id Oficina</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Id de la Oficina" name="_id" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Selecionar Area a la que pertenece</Form.Label>
                                        <Form.Control required as="select" name="id_local" onChange={actualizarState}>
                                            {
                                                area.map(data => (
                                                    <option value={data._id}>{data.nombre_area}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Nombre</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese nombre" name="nombre_oficina" onChange={actualizarState} />
                                    </Form.Group>

                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button type="submit" className="color-guinda" variant="secondary">
                            Crear
                    </Button>
                        <Button className="color-cancelar" variant="primary" onClick={handleClose}>
                            Cancelar
                    </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}

export default ModalCrearOficinas