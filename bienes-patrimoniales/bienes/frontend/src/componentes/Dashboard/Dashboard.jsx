import React from 'react';
import { NavLink} from 'react-router-dom';
import './Dashboard.css';
import {Button} from 'react-bootstrap';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Image from 'react-bootstrap/Image'
import GestionEntidad from './GestionEntidad/GestionEntidad'
import GestionLocales from './GestionLocales/GestionLocales';
import GestionAreas from './GestionAreas/GestionAreas'
import GestionOficinas from './GestionOficinas/GestionOficinas';
import Confirmation from './Confirmacion';
import { useEffect } from 'react';

const Dashboard = () => {
    const [show, setShow] = React.useState(false);
    const [userState, setUserState] = React.useState({ user: {}, error: null, authenticated: false })

    useEffect(() => {
        fetch("http://localhost:4000/auth/login/success", {
            method: "GET",
            credentials: "include",
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                "Access-Control-Allow-Credentials": true
            }
        })
            .then(response => {
                if (response.status === 200) {
                    console.log({ "de": "response", response });
                    return response.json();
                }
                throw new Error("failed to authenticate user");
            })
            .then(responseJson => {
                console.log({ "de": "responseJson", responseJson });
                setUserState({
                    authenticated: true,
                    user: responseJson.user
                });
            })
            .catch(error => {
                console.log({ "de": "catch", userState });
                setUserState({
                    authenticated: false,
                    error: "Failed to authenticate user"
                });
            });
    }, []);

    const _handleNotAuthenticated = () => {
        setUserState({
            authenticated: false
        })
    };

    const _handleLogoutClick = () => {
        // Set authenticated state to false in the HomePage
        fetch("http://localhost:4000/auth/logout",{method:'POST', body:JSON.stringify(
            {
                user:userState.user
            }), headers:{'Content-type':'application/json'}})
            .then((response)=> response.json())
            .then((data)=>{
                window.open("http://localhost:4000/auth/logout", "_self");
                _handleNotAuthenticated();                         
            })
    };

    return (

        <Router>
            <Confirmation usuario={userState.user} authenticated={userState.authenticated}/>

            <section id="Nav-left" className={`${show ? 'active-menu-toggle' : ''} px-3 color-guinda-claro `}>

                <div className="usuario mt-5 mb-4">
                    <Image id="imagen" src={userState.user.avatar_url} />
                </div>

                <div className="text-center text-white my-3"><h5>Bienvenido {userState.user.nombre}</h5></div>
                <div className="text-center text-white my-3">  <h6>Administrador</h6>   </div>

                <NavLink to={`/gestion-entidad`} activeClassName="active-Nav-left" className="enlace-menu py-2 d-block ">Gestión de Entidad</NavLink>
                <NavLink to={`/gestion-locales`} activeClassName="active-Nav-left" className="enlace-menu py-2 d-block">Gestión de Locales</NavLink>
                <NavLink to={`/gestion-areas`} activeClassName="active-Nav-left" className="enlace-menu  py-2 d-block">Gestión de Areas</NavLink>
                <NavLink to={`/gestion-oficinas`} activeClassName="active-Nav-left" className="enlace-menu py-2 d-block">Gestión de Oficinas</NavLink>
                <NavLink to={`/gestion-bienes`} activeClassName="active-Nav-left" className="enlace-menu py-2 d-block">Gestión de Bienes</NavLink>
                <NavLink to={`/gestion-usuarios`} activeClassName="active-Nav-left" className="enlace-menu  py-2 d-block">Gestión de Usuarios</NavLink>
                <NavLink to={`/gestion-catalogo`} activeClassName="active-Nav-left" className="enlace-menu py-2 d-block">Gestión de Catalogo</NavLink>
                <NavLink to={`/gestion-reportes`} activeClassName="active-Nav-left" className="enlace-menu py-2 d-block">Gestión de Reportes</NavLink>
                <NavLink to={`/gestion-servicios`} activeClassName="active-Nav-left" className="enlace-menu py-2 d-block">Gestión de Servicios</NavLink>


            </section>
            <section id="Main-content" className={`${show ? 'width-calc' : ''} `} >
                <section id="Nav-top" className="px-3 px-4 d-flex align-items-center justify-content-between ">
                    <img src="icon-menu.png" alt="imagen" onClick={() => setShow(!show)} id="img-menu" width="45px" className="icon-visibilidad m-0" />
                    <form>
                        <Button onClick={_handleLogoutClick}>Cerrar Sesión</Button>
                    </form>
                    
                    

                </section>
                <section id="Main">

                    <Switch>
                        <Route exact path="/gestion-entidad" component={GestionEntidad} />
                        <Route exact path="/gestion-locales" component={GestionLocales} />
                        <Route exact path="/gestion-areas" component={GestionAreas} />
                        <Route exact path="/gestion-oficinas" component={GestionOficinas} />
                    </Switch>
                </section>
            </section>
        </Router>
    );
}

export default Dashboard 
