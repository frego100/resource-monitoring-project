import React, { useState,useEffect } from 'react'
import RowLocal from './RowLocal'
import ModalCrearLocales from './ModalCrearLocales'

import { Form, Table, Pagination } from 'react-bootstrap'


const GestionLocales = () => {

    const [locales, setLocales] = useState([]);

    useEffect(()=>{
        listarLocales();
    }, []);

    const listarLocales =()=>{
        const apiUrl = 'http://localhost:4000/api/locales';
        fetch(apiUrl)
            .then((response)=> response.json())
            .then((data) =>setLocales(data))
    };

    return (
        <>
            <div className="container">

                <div className="row pt-5 pb-1">
                    <div className="col-10">
                        <h3>Gestion Locales</h3>
                    </div>
                </div>

                <div className="row py-4">
                    <div className="col-4">
                        <Form>
                            <Form.Group controlId="formBasicEmail" className="m-0">
                                <Form.Control type="email" placeholder="Buscar Local" />
                            </Form.Group>
                        </Form>
                    </div>

                    <div className="col-4 offset-4 text-right">
                        <ModalCrearLocales listarLocales={listarLocales}/>
                        
                    </div>
                </div>


                <div className="row py-4">
                    <div className="col">
                        <Table responsive className="tabla-color">
                            <thead>
                                <tr>
                                    <th>Nombre</th>
                                    <th>Dirección</th>
                                    <th>Propiedad</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                {locales.map( data => (
                                    
                                    <RowLocal 
                                        local={data} 
                                        key={data.id}
                                        listarLocales ={listarLocales}
                                    />
                                ))}
                            </tbody>
                        </Table>
                    </div>
                </div>

                <div className="col-12 my-4">
                    <div className="btn-toolbar justify-content-center" role="toolbar" aria-label="Toolbar with button groups">
                        <div className="btn-group mr-2" role="group" aria-label="First group">
                            <Pagination>
                                <Pagination.Item>{1}</Pagination.Item>
                                <Pagination.Item>{2}</Pagination.Item>
                                <Pagination.Item active>{3}</Pagination.Item>
                                <Pagination.Item>{4}</Pagination.Item>
                                <Pagination.Item >{5}</Pagination.Item>
                            </Pagination>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default GestionLocales
