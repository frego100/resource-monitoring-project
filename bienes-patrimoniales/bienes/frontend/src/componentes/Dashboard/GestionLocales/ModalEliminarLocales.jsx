import React from 'react'
import { Button, Modal } from 'react-bootstrap'

function ModalEliminarLocales({id, listarLocales}) {

    const [show, setShow] = React.useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const eliminarLocal=() =>{
        const apiUrl='http://localhost:4000/api/locales/'+id;
        fetch(apiUrl, {method:'DELETE'})
            .then((response) => response.json())
            .then((data)=>{
                console.log('this is your data', data);
                listarLocales();
            })
    }

    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Eliminar
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Eliminar Local</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <div className="row py-3">
                            <div className="col-12">
                                <h2>¿Desea eliminar el objeto?</h2>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="color-guinda" variant="secondary" onClick={()=>
                        {eliminarLocal(); handleClose();}}>
                        Eliminar
                    </Button>
                    <Button className="color-cancelar" variant="primary" onClick={handleClose}>
                        Cancelar
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );
}

export default ModalEliminarLocales