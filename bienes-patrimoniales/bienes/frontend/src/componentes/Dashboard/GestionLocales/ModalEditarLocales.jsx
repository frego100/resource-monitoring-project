import React, {useState, useEffect} from 'react'
import { Button, Modal, Form } from 'react-bootstrap'


function ModalEditarLocales({id, listarLocales}) {

    const [show, setShow] = React.useState(false);

    const [entidades, setEntidades] = useState([]);
    const [departamentos, setDepartamentos] = useState([]);
    const [provincias, setProvincias] = useState([]);
    const [distritos, setDistritos] = useState([]);
    const [tipCuenta, setTipCuenta] = useState([]);
    const [monedas, setMonedas] = useState([]);

    useEffect(() => {

        apiLocal();
        listarEntidades();
        listarDepartamentos();
        listarDistritos();
        listarMoneda();
        listarTipCuenta();
        listarProvincias();

    }, []);


    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const [local, setLocal] = React.useState({});

    const apiLocal=()=>{
        const apiUrl = 'http://localhost:4000/api/locales/'+id;
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setLocal(data));
    }
    
    const listarEntidades = () => {
        const apiUrl = 'http://localhost:4000/api/entidades';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setEntidades(data));
    };

    const listarDepartamentos =()=>{
        const apiUrl = 'http://localhost:4000/api/departamentos';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setDepartamentos(data));
    }

    const listarProvincias =()=>{
        const apiUrl = 'http://localhost:4000/api/provincias';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setProvincias(data));
    }

    const listarDistritos=()=>{
        const apiUrl = 'http://localhost:4000/api/distritos';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setDistritos(data));
    }

    const listarTipCuenta=()=>{
        const apiUrl = 'http://localhost:4000/api/tiposCuenta';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setTipCuenta(data));
    }

    const listarMoneda=()=>{
        const apiUrl = 'http://localhost:4000/api/monedas';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setMonedas(data));
    }

    const actualizarLocal = () => {

        const apiUrl = 'http://localhost:4000/api/locales/'+id;
        console.log(local)
        fetch(apiUrl, {method: 'PUT', body: JSON.stringify(local),headers:{'Content-type':'application/json'}   })
            .then((response) => response.json())
            .then((data) => {
                console.log('This is your data', data);
                listarLocales();
                console.log(listarLocales);
        });

    }

    const actualizarState = (e) => {
        setLocal({
            ...local,
            [e.target.name]: e.target.value
        })
    }

    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Editar Local
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Modal.Header closeButton>
                    <Modal.Title>Editar Local</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <div className="row py-3">
                            <div className="col-12">
                                <Form>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Nombre</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="nombre" defaultValue={local.nombre} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Entidad</Form.Label>
                                        <Form.Control as="select" name="id_entidad" onChange={actualizarState}>
                                            {
                                                entidades.map( data => (
                                                    <option value={data._id}>{data.nombre_entidad}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Direccion</Form.Label>
                                        <Form.Control as="textarea" rows="3" name="id_direccion" defaultValue={local.id_direccion} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Propiedad</Form.Label>
                                        <Form.Control as="textarea" rows="3" name="propiedad" defaultValue={local.propiedad} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Unidad de medida</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="unidad_medida" defaultValue={local.unidad_medida} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Departamento</Form.Label>
                                        <Form.Control as="select" name="id_departamento" defaultValue={local.id_departamento} onChange={actualizarState}>
                                            {
                                                departamentos.map( data => (
                                                    <option value={data._id}>{data.nombre}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Provincia</Form.Label>
                                        <Form.Control as="select" name="id_provincia" defaultValue={local.id_provincia} onChange={actualizarState}>
                                            {
                                                provincias.map( data => (
                                                    <option value={data._id}>{data.nombre_provincia}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Distrito</Form.Label>
                                        <Form.Control as="select" name="id_distrito" defaultValue={local.id_distrito} onChange={actualizarState}>
                                            {
                                                distritos.map( data => (
                                                    <option value={data._id}>{data.nombre_distrito}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Tipo de Cuenta</Form.Label>
                                        <Form.Control as="select" name="id_tipoCuenta" defaultValue={local.id_tipoCuenta} onChange={actualizarState}>
                                            {
                                                tipCuenta.map( data => (
                                                    <option value={data._id}>{data.descripcion}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Tipo de Moneda</Form.Label>
                                        <Form.Control as="select" name="id_moneda" defaultValue={local.id_moneda} onChange={actualizarState}>
                                            {
                                                monedas.map( data => (
                                                    <option value={data._id}>{data.nombre}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Cuenta</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="cuenta" defaultValue={local.cuenta} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Valor Contable</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="valor_contable" defaultValue={local.valor_contable} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Oficina Registral</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="oficina_registral" defaultValue={local.oficina_registral} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Tomo</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="tomo" defaultValue={local.tomo} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Fojas</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="fojas" defaultValue={local.fojas} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Asiento</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="asiento" defaultValue={local.asiento} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Codigo de Predio</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="cod_predio" defaultValue={local.cod_predio} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Partida electronica</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="partida_electronica" defaultValue={local.partida_electronica} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Partida sinabip</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="partida_sinabip" defaultValue={local.partida_sinabip} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Prop. registral</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="prop_registral" defaultValue={local.prop_registral} onChange={actualizarState} />
                                    </Form.Group>
                                    <Form.Group controlId="exampleForm.ControlTextarea1">
                                        <Form.Label>Beneficiario</Form.Label>
                                        <Form.Control as="textarea" rows="1" name="beneficiario" defaultValue={local.beneficiario} onChange={actualizarState} />
                                    </Form.Group>
                                </Form>
                            </div>
                        </div>
                    </div>
                </Modal.Body>
                <Modal.Footer>
                    <Button className="color-guinda"  variant="secondary" onClick={()=>{
                        handleClose();
                        actualizarLocal();}}>
                        Editar
                    </Button>
                    <Button className="color-cancelar" variant="primary" onClick={handleClose}>
                        Cancelar
                    </Button>
                </Modal.Footer>
            </Modal>
        </>
    );

}
export default ModalEditarLocales