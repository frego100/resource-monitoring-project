import React, { useState, useEffect } from 'react'
import { Button, Modal, Form } from 'react-bootstrap'

function ModalCrearLocales({ listarLocales }) {

    const [show, setShow] = React.useState(false);

    const [entidades, setEntidades] = useState([]);
    const [departamentos, setDepartamentos] = useState([]);
    const [provincias, setProvincias] = useState([]);
    const [distritos, setDistritos] = useState([]);
    const [tipCuenta, setTipCuenta] = useState([]);
    const [monedas, setMonedas] = useState([]);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    const [local, setLocal] = React.useState({});

    const [validated, setValidated] = React.useState(false);

    const handleSubmit = (event) => {
        const form = event.currentTarget;
        event.preventDefault();
        if (form.checkValidity() === false) {
            event.stopPropagation();
        } else {
            crearLocal();
            handleClose();
        }

        setValidated(true);
    };

    const crearLocal = () => {
        const apiUrl = "http://localhost:4000/api/locales";
        console.log(local)
        fetch(apiUrl, { method: 'POST', body: JSON.stringify(local), headers: { 'Content-type': 'application/json' } })
            .then((response) => response.json())
            .then((data) => {
                console.log('This is your data', data);
                listarLocales();
                console.log(listarLocales);
            })
    }

    useEffect(() => {
        listarEntidades();
        listarDepartamentos();
        listarDistritos();
        listarMoneda();
        listarTipCuenta();
        listarProvincias();
    }, []);

    const listarEntidades = () => {
        const apiUrl = 'http://localhost:4000/api/entidades';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setEntidades(data));
    };

    const listarDepartamentos = () => {
        const apiUrl = 'http://localhost:4000/api/departamentos';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setDepartamentos(data));
    }

    const listarProvincias = () => {
        const apiUrl = 'http://localhost:4000/api/provincias';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setProvincias(data));
    }

    const listarDistritos = () => {
        const apiUrl = 'http://localhost:4000/api/distritos';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setDistritos(data));
    }

    const listarTipCuenta = () => {
        const apiUrl = 'http://localhost:4000/api/tiposCuenta';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setTipCuenta(data));
    }

    const listarMoneda = () => {
        const apiUrl = 'http://localhost:4000/api/monedas';
        fetch(apiUrl)
            .then((response) => response.json())
            .then((data) => setMonedas(data));
    }

    const actualizarState = (e) => {
        setLocal({
            ...local,
            [e.target.name]: e.target.value
        })
    }

    return (
        <>
            <Button className="color-secundario" variant="primary" onClick={handleShow}>
                Crear Local
            </Button>

            <Modal
                show={show}
                onHide={handleClose}
                backdrop="static"
                keyboard={false}
            >
                <Form noValidate validated={validated} onSubmit={handleSubmit}>
                    <Modal.Header closeButton>
                        <Modal.Title>Crear Local</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>
                        <div className="container">
                            <div className="row py-3">
                                <div className="col-12">
                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Id Local</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Id del Local" name="_id" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Nombre</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese nombre" name="nombre_local" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Entidad</Form.Label>
                                        <Form.Control required as="select" name="id_entidad" onChange={actualizarState}>
                                            {
                                                entidades.map(data => (
                                                    <option value={data._id}>{data.nombre_entidad}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Direccion</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Direccion" name="id_direccion" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Propiedad</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Propiedad" name="propiedad" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Unidad de medida</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Unidad de medida" name="unidad_medida" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Departamento</Form.Label>
                                        <Form.Control required as="select" name="id_departamento" onChange={actualizarState}>
                                            {
                                                departamentos.map(data => (
                                                    <option value={data._id}>{data.nombre}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Provincia</Form.Label>
                                        <Form.Control required as="select" name="id_provincia" onChange={actualizarState}>
                                            {
                                                provincias.map(data => (
                                                    <option value={data._id}>{data.nombre_provincia}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>


                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Distrito</Form.Label>
                                        <Form.Control required as="select" name="id_distrito" onChange={actualizarState}>
                                            {
                                                distritos.map(data => (
                                                    <option value={data._id}>{data.nombre_distrito}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Tipo de Cuenta</Form.Label>
                                        <Form.Control required as="select" name="id_tipoCuenta" onChange={actualizarState}>
                                            {
                                                tipCuenta.map(data => (
                                                    <option value={data._id}>{data.descripcion}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlSelect1">
                                        <Form.Label>Tipo de Moneda</Form.Label>
                                        <Form.Control required as="select" name="id_local" onChange={actualizarState}>
                                            {
                                                monedas.map(data => (
                                                    <option value={data._id}>{data.nombre}</option>
                                                ))
                                            }
                                        </Form.Control>
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Cuenta</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Cuenta" name="cuenta" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Valor Contable</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Valor Contable" name="valor_contable" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Oficina Registral</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Oficina Registral" name="oficina_registral" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Tomo</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Tomo" name="tomo" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Fojas</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Fojas" name="fojas" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Asiento</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Asiento" name="asiento" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Codigo de Predio</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Codigo de Predio" name="cod_predio" onChange={actualizarState} />

                                    </Form.Group><Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Partida electronica</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Partida electronica" name="partida_electronica" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Partida sinabip</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Partida sinabip" name="partida_sinabif" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Prop. registral</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Prop. registral" name="prop_registral" onChange={actualizarState} />
                                    </Form.Group>

                                    <Form.Group controlId="exampleForm.ControlInput3">
                                        <Form.Label>Beneficiario</Form.Label>
                                        <Form.Control required type="text" placeholder="Ingrese Beneficiario" name="beneficiario" onChange={actualizarState} />
                                    </Form.Group>

                                </div>
                            </div>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button type="submit" className="color-guinda" variant="secondary">
                            Crear
                    </Button>
                        <Button className="color-cancelar" variant="primary" onClick={handleClose}>
                            Cancelar
                    </Button>
                    </Modal.Footer>
                </Form>
            </Modal>
        </>
    );
}


export default ModalCrearLocales
