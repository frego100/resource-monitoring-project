import React from 'react'
import ModalEliminarLocales from './ModalEliminarLocales'
import ModalEditarLocales from './ModalEditarLocales'

function RowLocal({local, listarLocales}) {
    
    return (
        <>
            <tr>
                <th>{local.nombre}</th>
                <th>{local.id_direccion}</th>
                <th>{local.propiedad}</th>
                <th className="text-center">
                    <ModalEliminarLocales id={local._id} listarLocales={listarLocales} /> 
                    <ModalEditarLocales id={local._id} listarLocales={listarLocales} /> 
                </th>
            </tr>
        </>
    );
}

export default RowLocal