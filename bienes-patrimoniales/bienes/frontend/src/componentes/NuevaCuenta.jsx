import React from 'react'
import { Link } from 'react-router-dom';

const NuevaCuenta = () => {
    return (
        <div>
            este es el componente grande del NuevaCuenta
            <div>
                <Link to={`/`} activeClassName="active">login</Link>
                <br/>
                <Link to={`/crear-cuenta`} activeClassName="active">crear cuenta</Link>
                <br/>
                <Link to={`/dashboard`} activeClassName="active">dashboard</Link>

            </div>
        </div>
    )
}

export default NuevaCuenta
