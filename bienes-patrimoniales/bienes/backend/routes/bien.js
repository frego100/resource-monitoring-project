var express = require('express');
var router = express.Router();

const bienes = require('../app/controllers/bien.controller.js');

// Bien's routes
router.post('/', bienes.create); // Create a new Bien
router.get('/', bienes.findAll); // Retrieve all Bien
router.get('/:id_bien', bienes.findOne); // Retrieve a single Bien with bienId
router.put('/:id_bien', bienes.update); // Update a Bien with bienId
router.delete('/:id_bien', bienes.delete); // Delete a Bien with bienId
module.exports = router;