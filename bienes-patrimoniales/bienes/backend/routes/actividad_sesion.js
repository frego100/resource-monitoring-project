var express = require('express');
var router = express.Router();

const actividadSesion = require('../app/controllers/actividad_sesion.controller');

router.post ('/', actividadSesion.create);
router.get ('/', actividadSesion.findAll);
router.get ('/:id_actividadSesion', actividadSesion.findOne);
router.put ('/:id_actividadSesion', actividadSesion.update);
router.delete ('/:id_actividadSesion', actividadSesion.delete);

module.exports=router;