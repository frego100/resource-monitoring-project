var express = require('express');
var router = express.Router();

const local = require('../app/controllers/local.controller');

router.post ('/', local.create);
router.get ('/', local.findAll);
router.get ('/:id_local', local.findOne);
router.put ('/:id_local', local.update);
router.delete ('/:id_local', local.delete);

module.exports=router;

