var express = require('express');
var router = express.Router();

const direccion = require('../app/controllers/direccion.controller');

router.post ('/', direccion.create);
router.get ('/', direccion.findAll);
router.get ('/:id_direccion', direccion.findOne);
router.put ('/:id_direccion', direccion.update);
router.delete ('/:id_direccion', direccion.delete);

module.exports=router;

