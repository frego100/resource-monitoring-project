var express = require('express');
var router = express.Router();

const tipo_doc_identidad = require('../app/controllers/tipo_doc_identidad.controller');

router.post ('/', tipo_doc_identidad.create);
router.get ('/', tipo_doc_identidad.findAll);
router.get ('/:id_tipo_doc_identidad', tipo_doc_identidad.findOne);
router.put ('/:id_tipo_doc_identidad', tipo_doc_identidad.update);
router.delete ('/:id_tipo_doc_identidad', tipo_doc_identidad.delete);

module.exports=router;