var express = require('express');
var router = express.Router();

const tipoCuenta = require('../app/controllers/tipo_cuenta.controller');

router.post ('/', tipoCuenta.create);
router.get ('/', tipoCuenta.findAll);
router.get ('/:id_tipoCuenta', tipoCuenta.findOne);
router.put ('/:id_tipoCuenta', tipoCuenta.update);
router.delete ('/:id_tipoCuenta', tipoCuenta.delete);

module.exports=router;

