var express = require('express');
var router = express.Router();

const distritos = require('../app/controllers/distrito.controller.js');

// Distrito's routes
router.post('/', distritos.create); // Create a new Distrito
router.get('/', distritos.findAll); // Retrieve all Distrito
router.get('/:id_distrito', distritos.findOne); // Retrieve a single Distrito with distritoId
router.put('/:id_distrito', distritos.update); // Update a Distrito with distritoId
router.delete('/:id_distrito', distritos.delete); // Delete a Distrito with distritoId
module.exports = router;