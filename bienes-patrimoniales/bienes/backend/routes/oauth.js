var express = require('express');
var router = express.Router();
const passport = require('passport');
const mongoose = require('mongoose');
const passportSetup = require('../config/passport-setup');
const HOME_PAGE = "http://localhost:3000/dashboard"
const ActividadSesion = require('../app/models/actividad_sesion.model');
const Usuario = require('../app/models/usuario.model');

/*router.use(passport.initialize());
router.use(passport.session());
*/

router.get('/google',
  passport.authenticate('google', { scope: ['profile', 'email'] }));

router.get('/google/callback',
  passport.authenticate('google'),
  function (req, res) {
    if (req.user.estadoActivo) {

      //Registro de la actividad_sesion
      var actividad = new ActividadSesion({
        id_usuario: req.user._id,
        fecha_inicio: new Date(),
      });
      actividad.save()
      .then(actividadSesion =>{
        Usuario.findByIdAndUpdate(req.user._id, { 
          $set: { 
            id_ultima_sesion: actividadSesion._id
          }
        },function(err){});
      });

      res.redirect(HOME_PAGE);
    } else {
      res.redirect('/auth/logout');
    }
    //console.log(req)
    //console.log(req.user);

  });

router.post('/logout', (req, res) => {
  //console.log(req.body);

  //Registrar la fecha_fin
  ActividadSesion.findByIdAndUpdate(req.body.user.id_ultima_sesion, { 
    $set: { 
      fecha_fin: new Date()
    }
  },function(err){});

  res.send({
    sesion: false
  });
});

router.get('/logout', (req, res) => {
  req.logOut();
  res.redirect('http://localhost:3000/');
});

router.get('/login/failed', (req, res) => {
  console.log('ha fallado');
  res.status(401).json({
    success: false,
    message: "User failed autenticate"
  })
});

router.get('/login/success', (req, res) => {
  if (req.user) {
    res.json({
      success: true,
      message: "autenticado",
      user: req.user,
      cookies: req.cookies
    });
  }
});

module.exports = router;
