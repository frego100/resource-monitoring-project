var express = require('express');
var router = express.Router();

const usuarios = require('../app/controllers/usuario.controller');

//Rutas para los usuarios
router.post('/', usuarios.create); 
router.get('/', usuarios.findAll);
router.get('/:_id', usuarios.findOne);
router.put('/:_id', usuarios.update);
router.delete('/:_id', usuarios.delete);

module.exports = router;