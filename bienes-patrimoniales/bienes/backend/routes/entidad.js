var express = require('express');
var router = express.Router();

const entidades = require('../app/controllers/entidad.controller.js');
const authCheck = (req, res, next) => {
    if (!req.user) {
      res.status(401).json({
        authenticated: false,
        message: "user has not been authenticated"
      });
    } else {
      next();
    }
  };
// Entidad's routes
router.post('/', entidades.create); // Create a new Entidad
router.get('/', entidades.findAll); // Retrieve all Entidad
router.get('/:id_entidad', entidades.findOne); // Retrieve a single Entidad with entidadId
router.put('/:id_entidad', entidades.update); // Update a Entidad with entidadId
router.delete('/:id_entidad', entidades.delete); // Delete a Entidad with entidadId
module.exports = router;