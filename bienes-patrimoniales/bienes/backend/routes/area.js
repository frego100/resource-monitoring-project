var express = require('express');
var router = express.Router();

const area = require('../app/controllers/area.controller');

router.post ('/', area.create);
router.get ('/', area.findAll);
router.get ('/:id_area', area.findOne);
router.put ('/:id_area', area.update);
router.delete ('/:id_area', area.delete);

module.exports=router;

