var express = require('express');
var router = express.Router();

const modalidad = require('../app/controllers/modalidad.controller');

router.post ('/', modalidad.create);
router.get ('/', modalidad.findAll);
router.get ('/:id_modalidad', modalidad.findOne);
router.put ('/:id_modalidad', modalidad.update);
router.delete ('/:id_modalidad', modalidad.delete);

module.exports=router;
