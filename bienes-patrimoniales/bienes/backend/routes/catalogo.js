var express = require('express');
var router = express.Router();

const catalogos = require('../app/controllers/catalogo.controller.js');

// Catalogo's routes
router.post('/', catalogos.create); // Create a new Catalogo
router.get('/', catalogos.findAll); // Retrieve all Catalogo
router.get('/:id_catalogo', catalogos.findOne); // Retrieve a single Catalogo with catalogoId
router.put('/:id_catalogo', catalogos.update); // Update a Catalogo with catalogoId
router.delete('/:id_catalogo', catalogos.delete); // Delete a Catalogo with catalogoId
module.exports = router;
