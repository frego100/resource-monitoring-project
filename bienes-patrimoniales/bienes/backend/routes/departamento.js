var express = require('express');
var router = express.Router();


const departamentos = require('../app/controllers/departmento.controller.js');
const authCheck = (req, res, next) => {
    if (!req.user) {
      res.status(401).json({
        authenticated: false,
        message: "user has not been authenticated"
      });
    } else {
      next();
    }
  };
// Department's routes
router.post('/', departamentos.create); // Create a new Department
router.get('/', departamentos.findAll); // Retrieve all Department
router.get('/:id_departamento', departamentos.findOne); // Retrieve a single Department with departmentId
router.put('/:id_departamento', departamentos.update); // Update a Department with departmentId
router.delete('/:id_departamento', departamentos.delete); // Delete a Department with departmentId
module.exports = router;