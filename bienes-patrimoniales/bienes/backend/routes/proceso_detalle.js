var express = require('express');
var router = express.Router();

const procesoDetalle = require('../app/controllers/proceso_detalle.controller');

router.post ('/', procesoDetalle.create);
router.get ('/', procesoDetalle.findAll);
router.get ('/:id_procesoDetalle', procesoDetalle.findOne);
router.put ('/:id_procesoDetalle', procesoDetalle.update);
router.delete ('/:id_procesoDetalle', procesoDetalle.delete);

module.exports=router;
