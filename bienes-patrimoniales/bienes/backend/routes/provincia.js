var express = require('express');
var router = express.Router();

const provincias = require('../app/controllers/provincia.controller.js');

// Provincia's routes
router.post('/', provincias.create); // Create a new Provincia
router.get('/', provincias.findAll); // Retrieve all Provincia
router.get('/:id_provincia', provincias.findOne); // Retrieve a single Provincia with provinciaId
router.put('/:id_provincia', provincias.update); // Update a Provincia with provinciaId
router.delete('/:id_provincia', provincias.delete); // Delete a Provincia with provinciaId
module.exports = router;