var express = require('express');
var router = express.Router();

const procesos = require('../app/controllers/proceso.controller');

router.post ('/', procesos.create);
router.get ('/', procesos.findAll);
router.get ('/:id_proceso', procesos.findOne);
router.put ('/:id_proceso', procesos.update);
router.delete ('/:id_proceso', procesos.delete);

module.exports=router;
