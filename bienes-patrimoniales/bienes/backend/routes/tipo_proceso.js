var express = require('express');
var router = express.Router();

const tipoProceso = require('../app/controllers/tipo_proceso.controller');

router.post ('/', tipoProceso.create);
router.get ('/', tipoProceso.findAll);
router.get ('/:id_tipoProceso', tipoProceso.findOne);
router.put ('/:id_tipoProceso', tipoProceso.update);
router.delete ('/:id_tipoProceso', tipoProceso.delete);

module.exports=router;
