var express = require('express');
var router = express.Router();

const moneda = require('../app/controllers/moneda.controller');

router.post ('/', moneda.create);
router.get ('/', moneda.findAll);
router.get ('/:id_moneda', moneda.findOne);
router.put ('/:id_moneda', moneda.update);
router.delete ('/:id_moneda', moneda.delete);

module.exports=router;

