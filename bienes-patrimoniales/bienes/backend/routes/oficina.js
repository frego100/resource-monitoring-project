var express = require('express');
var router = express.Router();

const oficina = require('../app/controllers/oficina.controller');

router.post ('/', oficina.create);
router.get ('/', oficina.findAll);
router.get ('/:id_oficina', oficina.findOne);
router.put ('/:id_oficina', oficina.update);
router.delete ('/:id_oficina', oficina.delete);

module.exports=router;