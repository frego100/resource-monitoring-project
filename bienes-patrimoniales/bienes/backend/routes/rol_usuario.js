var express = require('express');
var router = express.Router();

const rolUsuario = require('../app/controllers/rol_usuario.controller');

router.post ('/', rolUsuario.create);
router.get ('/', rolUsuario.findAll);
router.get ('/:id_rol_usuario', rolUsuario.findOne);
router.put ('/:id_rol_usuario', rolUsuario.update);
router.delete ('/:id_rol_usuario', rolUsuario.delete);

module.exports=router;