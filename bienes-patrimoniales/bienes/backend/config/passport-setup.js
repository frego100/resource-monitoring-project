const GoogleStrategy = require('passport-google-oauth20').Strategy;
const passport = require('passport');
const keys = require('./keys');
const mongoose = require('mongoose');
const Usuario = require('../app/models/usuario.model');
const router = require('express').Router();

  passport.use(
    new GoogleStrategy(
      {
        clientID: keys.google.GOOGLE_CLIENT_ID,
        clientSecret: keys.google.GOOGLE_CLIENT_SECRET,
        callbackURL: '/auth/google/callback',
      },
      async (accessToken, refreshToken, profile, done) => {
        //const newUser = {
          //_id: profile.id,
          /*displayName: profile.displayName,
          firstName: profile.name.givenName,
          lastName: profile.name.familyName,
          image: profile.photos[0].value,*/
        //}
        console.log(profile);
        try {
          let user = await Usuario.findOne({ email: profile._json.email })
          
          if (user) {
            //usuario existente
            //console.log("usuario existente");
            done(null, user)

          } else {
            var newUser=new Usuario({
              _id: profile.id,
              nombre:profile.displayName,
              email: profile._json.email,
              estadoActivo: false
            })
            //creando nuevo usuario
            //user = await Usuario.create(newUser)
            //console.log("Creando nuevo usuario");
            done(null, newUser)
          }
        } catch (err) {
          console.error(err)
        }
      }
    )
  )

  passport.serializeUser((user, done) => {
    done(null, user._id)
  })

  passport.deserializeUser((id, done) => {
    Usuario.findById(id).then((user)=>{
      done(null,user);
    });
  })
