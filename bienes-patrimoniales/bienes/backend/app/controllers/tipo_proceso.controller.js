const TipoProceso = require('../models/tipo_proceso.model.js');

// Create and Save a new TipoProceso
exports.create = (req, res) => {
    // Create a TipoProceso
    const _tipoProceso = new TipoProceso({
        _id: req.body._id,
        id_modalidad: req.body.id_modalidad,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion
    });
    let body = req.body;
    console.log(body);

    // Save TipoProceso the database
    _tipoProceso.save()
    .then(_tipoProceso => {
        res.send(_tipoProceso);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the TipoProceso."
        });
    });
};

// Retrieve and return all TiposProceso from the database.
exports.findAll = (req, res) => {
    TipoProceso.find()
    .then(tiposProceso => {
        res.send(tiposProceso);
        console.log(tiposProceso)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving tiposProceso."
        });
    });
};

// Find a single TipoProceso with a procesoId
exports.findOne = (req, res) => {
    TipoProceso.findById(req.params._id)
    .then(tipoProceso => {
        if(!tipoProceso) {
            return res.status(404).send({
                message: "tipoProceso not found with id " + req.params._id
            });
        }
        res.send(tipoProceso);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "tipoProceso not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error retrieving tipoProceso with id " + req.params._id
        });
    });
};

// Update an TipoProceso identified by the tipoProcesoID in the request
exports.update = (req, res) => {
    // Find TipoProceso and update it with the request body
    TipoProceso.findByIdAndUpdate(req.params._id, {
          id_modalidad: req.body.id_modalidad,
          nombre: req.body.nombre,
          descripcion: req.body.descripcion
    }, {new: true})
    .then(tipoProceso => {
        if(!tipoProceso) {
            return res.status(404).send({
                message: "tipoProceso not found with id " + req.params._id
            });
        }
        res.send(tipoProceso);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "tipoProceso not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error updating tipoProceso with id " + req.params._id
        });
    });
};

// Delete a TipoProceso with the specified tipoProcesoId in the request
exports.delete = (req, res) => {
    TipoProceso.findByIdAndRemove(req.params._id)
    .then(tipoProceso => {
        if(!tipoProceso) {
            return res.status(404).send({
                message: "tipoProceso not found with id " + req.params._id
            });
        }
        res.send({message: "tipoProceso deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "tipoProceso not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Could not delete tipoProceso with id " + req.params._id
        });
    });
};
