const Area = require('../models/area.model');
const Local = require('../models/local.model')

exports.create = (req,res) =>{
    const area = new Area({
        _id: req.body._id,
        id_local: req.body.id_local,
        nombre_area: req.body.nombre_area,
        siglas_area: req.body.siglas_area
    });
    let body = req.body;
    console.log(body);

    area.save()
    .then(area =>{
        res.send(area);
    }).catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Area."
        });
    })
};

exports.findAll = (req, res) =>{
    /*Area.find({}, function(err, area){
        Local.populate(area, {path: "id_local"}, function(err, area){
            res.status(200).send(area);
        });

    });*/ 
    Area.find()
    .then(area=> {
        res.send(area);
        console.log(area)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving areas."
        });
    });
};

exports.findOne = (req, res) =>{
    Area.findById(req.params.id_area)
    .then(area => {
        if(!area) {
            return res.status(404).send({
                message: "Area not found with id " + req.params.id_area
            });
        }
        res.send(area);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Area not found with id " + req.params.id_area
            });
        }
        return res.status(500).send({
            message: "Error retrieving area with id " + req.params.id_area
        });
    });
};

exports.update = (req, res) =>{
    Area.findByIdAndUpdate(req.params.id_area,{
        id_local: req.body.id_local,
        nombre_area: req.body.nombre_area,
        siglas_area: req.body.siglas_area
    }, {new:true})
    .then(area => {
        if(!area) {
            return res.status(404).send({
                message: "Area not found with id " + req.params.id_area
            });
        }
        res.send(area);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Area not found with id " + req.params.id_area
            });
        }
        return res.status(500).send({
            message: "Error updating area with id " + req.params.id_area
        });
    });

};

exports.delete = (req, res) =>{
    Area.findByIdAndRemove(req.params.id_area)
    .then(area => {
        if(!area) {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_area
            });
        }
        res.send({message: "Area deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Area not found with id " + req.params.id_area
            });
        }
        return res.status(500).send({
            message: "Could not delete area with id " + req.params.id_area
        });
    });

};