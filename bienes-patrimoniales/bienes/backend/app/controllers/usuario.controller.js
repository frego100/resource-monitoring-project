const Usuario = require('../models/usuario.model');

exports.create = (req, res) =>{
    const usuario = new Usuario({
        _id: req.body._id,
        id_tipo_doc_identidad: req.body.id_tipo_doc_identidad,
        id_area: req.body.id_area,
        id_oficina: req.body.id_oficina,
        id_local: req.body.id_local,
        id_rol_usuario: req.body.id_rol_usuario,
        nombre: req.body.nombre,
        email: req.body.email,
        modalidad: req.body.modalidad,
        avatar_url: req.body.avatar_url,
        estadoActivo: true
    });
    let body = req.body;
    console.log(body);

    usuario.save()
    .then(usuario =>{
        res.send(usuario);
    }).catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occurred while creating the user."
        });
    })
};

exports.findAll = (req, res) =>{
    Usuario.find()
    .then(usuario=> {
        res.send(usuario);
        console.log(usuario)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving users."
        });
    });    
};

exports.findOne = (req, res) =>{
    Usuario.findById(req.params._id)
    .then(usuario => {
        if(!usuario) {
            return res.status(404).send({
                message: "User not found with id " + req.params._id
            });
        }
        res.send(usuario);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error retrieving user with id " + req.params._id
        });
    });
    
};

exports.update = (req, res) =>{
    Usuario.findByIdAndUpdate(req.params._id,{
        id_tipo_doc_identidad: req.body.id_tipo_doc_identidad,
        id_area: req.body.id_area,
        id_oficina: req.body.id_oficina,
        id_local: req.body.id_local,
        id_rol_usuario: req.body.id_rol_usuario,
        email:req.body.email,
        nombre: req.body.nombre,
        modalidad: req.body.modalidad,
        avatar_url: req.body.avatar_url,
        estadoActivo: true
    }, {new: true})
    .then(usuario => {
        if(!usuario) {
            return res.status(404).send({
                message: "User not found with id " + req.params._id
            });
        }
        res.send(usuario);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "User not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error updating user with id " + req.params._id,
            info: "Error type " + err
        });
    });
    
};

exports.delete = (req, res) =>{
    Usuario.findByIdAndRemove(req.params._id)
    .then(usuario => {
        if(!usuario) {
            return res.status(404).send({
                message: "User not found with id " + req.params._id
            });
        }
        res.send({message: "User deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "User not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Could not delete usuer with id " + req.params._id
        });
    });    
};