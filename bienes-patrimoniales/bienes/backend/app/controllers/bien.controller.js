const Bien = require('../models/bien.model.js');

// Create and Save a new Bien
exports.create = (req, res) => {
    // Create a Department
    const _bien = new Bien({
        _id: req.body._id, //código patrimonial
        cod_interno: req.body.cod_interno,
        id_catalogo: req.body.id_catalogo,
        denominacion: req.body.denominacion,
        resulocion_alta: req.body.resulocion_alta,
        marca: req.body.marca,
        modelo: req.body.modelo,
        tipo: req.body.tipo,
        serie: req.body.serie,
        dimensiones: req.body.dimensiones,
        estado: req.body.estado,
        cuenta_contable: req.body.cuenta_contable,
        valor_neto: req.body.valor_neto,
        causal_alta: req.body.causal_alta,
        causal_baja: req.body.causal_baja,
        acto_disposicion: req.body.acto_disposicion,
        valor_tasacion: req.body.valor_tasacion,
        ubicacion: req.body.ubicacion,
        tiempo_permanencia: req.body.tiempo_permanencia
    });
    let body = req.body;
    console.log(body);

    // Save Bien in the database
    _bien.save()
    .then(bien => {
        res.send(bien);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Bien."
        });
    });
};

// Retrieve and return all bienes from the database.
exports.findAll = (req, res) => {
    Bien.find()
    .then(bienes => {
        res.send(bienes);
        console.log(bienes)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving bienes."
        });
    });
};

// Find a single bien with a bienId
exports.findOne = (req, res) => {
    Bien.findById(req.params.id_bien)
    .then(bien => {
        if(!bien) {
            return res.status(404).send({
                message: "Bien not found with id " + req.params.id_bien
            });
        }
        res.send(bien);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Bien not found with id " + req.params.id_bien
            });
        }
        return res.status(500).send({
            message: "Error retrieving bien with id " + req.params.id_bien
        });
    });
};

// Update an bien identified by the bienId in the request
exports.update = (req, res) => {
    // Find bien and update it with the request body
    Bien.findByIdAndUpdate(req.params.id_bien, {
        cod_patrimonial: req.body.cod_patrimonial,
        cod_interno: req.body.cod_interno,
        id_catalogo: req.body.id_catalogo,
        denominacion: req.body.denominacion,
        resulocion_alta: req.body.resulocion_alta,
        marca: req.body.marca,
        modelo: req.body.modelo,
        tipo: req.body.tipo,
        serie: req.body.serie,
        dimensiones: req.body.dimensiones,
        estado: req.body.estado,
        cuenta_contable: req.body.cuenta_contable,
        valor_neto: req.body.valor_neto,
        causal_alta: req.body.causal_alta,
        causal_baja: req.body.causal_baja,
        acto_disposicion: req.body.acto_disposicion,
        valor_tasacion: req.body.valor_tasacion,
        ubicacion: req.body.ubicacion,
        tiempo_permanencia: req.body.tiempo_permanencia
    }, {new: true})
    .then(bien => {
        if(!bien) {
            return res.status(404).send({
                message: "Bien not found with id " + req.params.id_bien
            });
        }
        res.send(bien);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Bien not found with id " + req.params.id_bien
            });
        }
        return res.status(500).send({
            message: "Error updating bien with id " + req.params.id_bien
        });
    });
};

// Delete a bien with the specified bienId in the request
exports.delete = (req, res) => {
    Bien.findByIdAndRemove(req.params.id_bien)
    .then(bien => {
        if(!bien) {
            return res.status(404).send({
                message: "Bien not found with id " + req.params.id_bien
            });
        }
        res.send({message: "Bien deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Bien not found with id " + req.params.id_bien
            });
        }
        return res.status(500).send({
            message: "Could not delete bien with id " + req.params.id_bien
        });
    });
};
