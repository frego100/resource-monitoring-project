const ActividadSesion = require('../models/actividad_sesion.model');
const Usuario = require('../models/usuario.model');

exports.create = (req,res) =>{
    const actividadSesion = new ActividadSesion({
        //_id: req.body._id,
        id_usuario: req.body.id_usuario,
        fecha_inicio: req.body.fecha_inicio,
        fecha_fin: req.body.fecha_fin,
        hora_inicio: req.body.hora_inicio,
        hora_fin: req.body.hora_fin,
        estado: req.body.estado
    });
    let body = req.body;
    console.log(body);

    actividadSesion.save()
    .then(actividadSesion =>{
        res.send(actividadSesion);
    }).catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occurred while creating the ActividadSesion."
        });
    })
};

exports.findAll = (req, res) =>{
    /*ActividadSesion.find({}, function(err, area){
        Usuario.populate(area, {path: "id_usuario"}, function(err, area){
            res.status(200).send(area);
        });

    });*/
    ActividadSesion.find()
    .then(actividadSesion=> {
        res.send(actividadSesion);
        console.log(actividadSesion)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving actividadSesions."
        });
    });
};

exports.findOne = (req, res) =>{
    ActividadSesion.findById(req.params.id_actividadSesion)
    .then(actividadSesion => {
        if(!actividadSesion) {
            return res.status(404).send({
                message: "ActividadSesion not found with id " + req.params.id_actividadSesion
            });
        }
        res.send(actividadSesion);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "ActividadSesion not found with id " + req.params.id_actividadSesion
            });
        }
        return res.status(500).send({
            message: "Error retrieving actividadSesion with id " + req.params.id_actividadSesion
        });
    });
};

exports.update = (req, res) =>{
    ActividadSesion.findByIdAndUpdate(req.params.id_actividadSesion,{
        id_usuario: req.body.id_usuario,
        fecha_inicio: req.body.fecha_inicio,
        fecha_fin: req.body.fecha_fin,
        hora_inicio: req-body.hora_inicio,
        hora_fin: req.body.hora_fin,
        estado: req.body.estado
    }, {new:true})
    .then(actividadSesion => {
        if(!actividadSesion) {
            return res.status(404).send({
                message: "ActividadSesion not found with id " + req.params.id_actividadSesion
            });
        }
        res.send(actividadSesion);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "ActividadSesion not found with id " + req.params.id_actividadSesion
            });
        }
        return res.status(500).send({
            message: "Error updating actividadSesion with id " + req.params.id_actividadSesion
        });
    });

};

exports.delete = (req, res) =>{
    ActividadSesion.findByIdAndRemove(req.params.id_actividadSesion)
    .then(actividadSesion => {
        if(!actividadSesion) {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_actividadSesion
            });
        }
        res.send({message: "ActividadSesion deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "ActividadSesion not found with id " + req.params.id_actividadSesion
            });
        }
        return res.status(500).send({
            message: "Could not delete actividadSesion with id " + req.params.id_actividadSesion
        });
    });

};