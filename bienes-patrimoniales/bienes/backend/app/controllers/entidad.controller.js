const Entidad = require('../models/entidad.model.js');

// Create and Save a new Entidad
exports.create = (req, res) => {
    // Create a Entidad
    const _entidad = new Entidad({
        _id: req.body._id,
        nombre_entidad: req.body.nombre_entidad,
        dependencia: req.body.dependencia
    });
    let body = req.body;
    console.log(body);

    // Save Entidad the database
    _entidad.save()
    .then(entidad => {
        res.send(entidad);
    }).catch(err => {
        console.log(err)
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Entidad."
        });
    });
};

// Retrieve and return all entidades from the database.
exports.findAll = (req, res) => {
    Entidad.find()
    .then(entidades => {
        res.send(entidades);
        console.log(entidades)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving entidades."
        });
    });
};

// Find a single entidad with a entidadId
exports.findOne = (req, res) => {
    Entidad.findById(req.params.id_entidad)
    .then(entidad => {
        if(!entidad) {
            return res.status(404).send({
                message: "Entidad not found with id " + req.params.id_entidad
            });
        }
        res.send(entidad);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Entidad not found with id " + req.params.id_entidad
            });
        }
        return res.status(500).send({
            message: "Error retrieving entidad with id " + req.params.id_entidad
        });
    });
};

// Update an entidad identified by the entidadId in the request
exports.update = (req, res) => {
    // Find entidad and update it with the request body
    Entidad.findByIdAndUpdate(req.params.id_entidad, {
        nombre_entidad: req.body.nombre_entidad,
        dependencia: req.body.dependencia
    }, {new: true})
    .then(entidad => {
        if(!entidad) {
            return res.status(404).send({
                message: "Entidad not found with id " + req.params.id_entidad
            });
        }
        res.send(entidad);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Entidad not found with id " + req.params.id_entidad
            });
        }
        return res.status(500).send({
            message: "Error updating entidad with id " + req.params.id_entidad
        });
    });
};

// Delete a entidad with the specified entidadId in the request
exports.delete = (req, res) => {
    Entidad.findByIdAndRemove(req.params.id_entidad)
    .then(entidad => {
        if(!entidad) {
            return res.status(404).send({
                message: "Entidad not found with id " + req.params.id_entidad
            });
        }
        res.send({message: "Entidad deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Entidad not found with id " + req.params.id_entidad
            });
        }
        return res.status(500).send({
            message: "Could not delete entidad with id " + req.params.id_entidad
        });
    });
};
