const Proceso = require('../models/proceso.model.js');

// Create and Save a new Proceso
exports.create = (req, res) => {
    // Create a Proceso
    const _proceso = new Proceso({
        _id: req.body._id,// num_resolucion
        tipo_proceso: req.body.tipo_proceso,
        id_actividad_sesion: req.body.id_actividad_sesion
    });
    let body = req.body;
    console.log(body);

    // Save Proceso the database
    _proceso.save()
    .then(_proceso => {
        res.send(_proceso);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Proceso."
        });
    });
};

// Retrieve and return all procesos from the database.
exports.findAll = (req, res) => {
    Proceso.find()
    .then(procesos => {
        res.send(procesos);
        console.log(procesos)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving procesos."
        });
    });
};

// Find a single proceso with a procesoId
exports.findOne = (req, res) => {
    Proceso.findById(req.params._id)
    .then(proceso => {
        if(!proceso) {
            return res.status(404).send({
                message: "Proceso not found with id " + req.params._id
            });
        }
        res.send(proceso);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Proceso not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error retrieving proceso with id " + req.params._id
        });
    });
};

// Update an proceso identified by the procesoID in the request
exports.update = (req, res) => {
    // Find proceso and update it with the request body
    Proceso.findByIdAndUpdate(req.params._id, {
        tipo_proceso: req.body.tipo_proceso,
        id_actividad_sesion: req.body.id_actividad_sesion
    }, {new: true})
    .then(proceso => {
        if(!proceso) {
            return res.status(404).send({
                message: "Proceso not found with id " + req.params._id
            });
        }
        res.send(proceso);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Proceso not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error updating proceso with id " + req.params._id
        });
    });
};

// Delete a proceso with the specified procesoId in the request
exports.delete = (req, res) => {
    Proceso.findByIdAndRemove(req.params._id)
    .then(proceso => {
        if(!proceso) {
            return res.status(404).send({
                message: "Proceso not found with id " + req.params._id
            });
        }
        res.send({message: "Proceso deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Proceso not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Could not delete proceso with id " + req.params._id
        });
    });
};
