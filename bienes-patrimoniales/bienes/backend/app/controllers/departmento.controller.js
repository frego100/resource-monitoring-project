const Department = require('../models/departamento.model.js');
const Provincia = require('../models/provincia.model');
const Distritos = require('../models/distrito.model');
const { Schema } = require('mongoose');

// Create and Save a new Department
exports.create = (req, res) => {
    // Create a Department
    const departamento = new Department({
        _id: req.body._id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        estadoActivo: true
    });
    let body = req.body;
    console.log(body);

    // Save Department in the database
    departamento.save()
    .then(department => {
        res.send(department);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Department."
        });
    });
};

// Retrieve and return all departments from the database.
exports.findAll = (req, res) => {
    Department.find()
    .then(departments=> {
        res.send(departments);
        console.log(departments)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving departments."
        });
    });
};

// Find a single department with a departmentId
exports.findOne = (req, res) => {
    Department.findById(req.params.id_departamento)
    .then(department => {
        if(!department) {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_departamento
            });
        }
        res.send(department);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_departamento
            });
        }
        return res.status(500).send({
            message: "Error retrieving department with id " + req.params.id_departamento
        });
    });
};

// Update an department identified by the departmentId in the request
exports.update = (req, res) => {
    // Find department and update it with the request body
    Department.findByIdAndUpdate(req.params.id_departamento, {
        nombre: req.body.nombre,
        descripcion: req.body.descripcion,
        estadoActivo: true
    }, {new: true})
    .then(department => {
        if(!department) {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_departamento
            });
        }
        res.send(department);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_departamento
            });
        }
        return res.status(500).send({
            message: "Error updating department with id " + req.params.id_departamento
        });
    });
};

// Delete a department with the specified departmentId in the request
exports.delete = (req, res) => {
    Department.findByIdAndRemove(req.params.id_departamento)
    .then(department => {
        if(!department) {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_departamento
            });
        }
        res.send({message: "Department deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_departamento
            });
        }
        return res.status(500).send({
            message: "Could not delete department with id " + req.params.id_departamento
        });
    });
};
