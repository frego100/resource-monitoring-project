const RolUsuario = require('../models/rol_usuario.model');
const { Schema } = require('mongoose');

// Create and Save a new Rol user
exports.create = (req, res) => {
    // Create a Rol user
    const rolUsuario = new RolUsuario({
        _id: req.body._id,
        descripcion: req.body.descripcion,
        estado: req.body.estado,
        estadoActivo: true
    });
    let body = req.body;
    console.log(body);

    // Save office in the type of document
    rolUsuario.save()
    .then(rolUsuario => {
        res.send(rolUsuario);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Rol user."
        });
    });
};

// Retrieve and return all type of document from the database.
exports.findAll = (req, res) => {
    RolUsuario.find()
    .then(rolUsuario=> {
        res.send(rolUsuario);
        console.log(rolUsuario)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving Rol user"
        });
    });
};

// Find a single department with a departmentId
exports.findOne = (req, res) => {
    RolUsuario.findById(req.params.id_rol_usuario)
    .then(rolUsuario => {
        if(!rolUsuario) {
            return res.status(404).send({
                message: "Rol user not found with id " + req.params.id_rol_usuario
            });
        }
        res.send(rolUsuario);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Rol user not found with id " + req.params.id_rol_usuario
            });
        }
        return res.status(500).send({
            message: "Error retrieving Rol user with id " + req.params.id_rol_usuario
        });
    });
};

// Update an department identified by the departmentId in the request
exports.update = (req, res) => {
    // Find department and update it with the request body
    RolUsuario.findByIdAndUpdate(req.params.id_rol_usuario, {
        descripcion: req.body.descripcion,
        estado: req.body.estado,
        estadoActivo: true
    }, {new: true})
    .then(rolUsuario => {
        if(!rolUsuario) {
            return res.status(404).send({
                message: "Rol user not found with id " + req.params.id_rol_usuario
            });
        }
        res.send(rolUsuario);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Rol user not found with id " + req.params.id_rol_usuario
            });
        }
        return res.status(500).send({
            message: "Error updating Rol user with id " + req.params.id_rol_usuario
        });
    });
};

// Delete a department with the specified departmentId in the request
exports.delete = (req, res) => {
    RolUsuario.findByIdAndRemove(req.params.id_rol_usuario)
    .then(rolUsuario => {
        if(!rolUsuario) {
            return res.status(404).send({
                message: "Rol user not found with id " + req.params.id_rol_usuario
            });
        }
        res.send({message: "Rol user deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Rol user not found with id " + req.params.id_rol_usuario
            });
        }
        return res.status(500).send({
            message: "Could not delete Rol user with id " + req.params.id_rol_usuario
        });
    });
};