const TipoDocIdentidad = require('../models/tipo_doc_identidad.model');
const { Schema } = require('mongoose');

// Create and Save a new type of document
exports.create = (req, res) => {
    // Create a office
    const tipoDocIdentidad = new TipoDocIdentidad({
        _id: req.body._id,
        descripcion: req.body.descripcion,
        estado: req.body.estado,
        estadoActivo: true
    });
    let body = req.body;
    console.log(body);

    // Save office in the type of document
    tipoDocIdentidad.save()
    .then(tipodoc => {
        res.send(tipodoc);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the type of document."
        });
    });
};

// Retrieve and return all type of document from the database.
exports.findAll = (req, res) => {
    TipoDocIdentidad.find()
    .then(tipodoc=> {
        res.send(tipodoc);
        console.log(tipodoc)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving type of document."
        });
    });
};

// Find a single department with a departmentId
exports.findOne = (req, res) => {
    TipoDocIdentidad.findById(req.params.id_tipo_doc_identidad)
    .then(tipodoc => {
        if(!tipodoc) {
            return res.status(404).send({
                message: "Type of document not found with id " + req.params.id_tipo_doc_identidad
            });
        }
        res.send(tipodoc);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "type of document not found with id " + req.params.id_tipo_doc_identidad
            });
        }
        return res.status(500).send({
            message: "Error retrieving type of document with id " + req.params.id_tipo_doc_identidad
        });
    });
};

// Update an department identified by the departmentId in the request
exports.update = (req, res) => {
    // Find department and update it with the request body
    TipoDocIdentidad.findByIdAndUpdate(req.params.id_tipo_doc_identidad, {
        descripcion: req.body.descripcion,
        estado: req.body.estado,
        estadoActivo: true
    }, {new: true})
    .then(tipodoc => {
        if(!tipodoc) {
            return res.status(404).send({
                message: "type of document not found with id " + req.params.id_tipo_doc_identidad
            });
        }
        res.send(tipodoc);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "type of document not found with id " + req.params.id_tipo_doc_identidad
            });
        }
        return res.status(500).send({
            message: "Error updating type of document with id " + req.params.id_tipo_doc_identidad
        });
    });
};

// Delete a department with the specified departmentId in the request
exports.delete = (req, res) => {
    TipoDocIdentidad.findByIdAndRemove(req.params.id_tipo_doc_identidad)
    .then(tipodoc => {
        if(!tipodoc) {
            return res.status(404).send({
                message: "type of document not found with id " + req.params.id_tipo_doc_identidad
            });
        }
        res.send({message: "type of document deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "type of document not found with id " + req.params.id_tipo_doc_identidad
            });
        }
        return res.status(500).send({
            message: "Could not delete type of document with id " + req.params.id_tipo_doc_identidad
        });
    });
};