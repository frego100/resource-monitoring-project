const Provincia = require('../models/provincia.model.js');

// Create and Save a new Provincia
exports.create = (req, res) => {
    // Create a Provincia
    const _provincia = new Provincia({
        _id: req.body._id,
        nombre_provincia: req.body.nombre_provincia,
        descripcion: req.body.descripcion
    });
    let body = req.body;
    console.log(body);

    // Save Provincia the database
    _provincia.save()
    .then(provincia => {
        res.send(provincia);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Provincia."
        });
    });
};

// Retrieve and return all provincias from the database.
exports.findAll = (req, res) => {
    Provincia.find()
    .then(provincias => {
        res.send(provincias);
        console.log(provincias)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving provincias."
        });
    });
};

// Find a single provincia with a provinciaId
exports.findOne = (req, res) => {
    Provincia.findById(req.params.id_provincia)
    .then(provincia => {
        if(!provincia) {
            return res.status(404).send({
                message: "Provincia not found with id " + req.params.id_provincia
            });
        }
        res.send(provincia);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Provincia not found with id " + req.params.id_provincia
            });
        }
        return res.status(500).send({
            message: "Error retrieving provincia with id " + req.params.id_provincia
        });
    });
};

// Update an provincia identified by the provinciaId in the request
exports.update = (req, res) => {
    // Find provincia and update it with the request body
    Provincia.findByIdAndUpdate(req.params.id_provincia, {
        nombre_provincia: req.body.nombre_provincia,
        descripcion: req.body.descripcion
    }, {new: true})
    .then(provincia => {
        if(!provincia) {
            return res.status(404).send({
                message: "Provincia not found with id " + req.params.id_provincia
            });
        }
        res.send(provincia);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Provincia not found with id " + req.params.id_provincia
            });
        }
        return res.status(500).send({
            message: "Error updating provincia with id " + req.params.id_provincia
        });
    });
};

// Delete a provincia with the specified provinciaId in the request
exports.delete = (req, res) => {
    Provincia.findByIdAndRemove(req.params.id_provincia)
    .then(provincia => {
        if(!provincia) {
            return res.status(404).send({
                message: "Provincia not found with id " + req.params.id_provincia
            });
        }
        res.send({message: "Provincia deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Provincia not found with id " + req.params.id_provincia
            });
        }
        return res.status(500).send({
            message: "Could not delete provincia with id " + req.params.id_provincia
        });
    });
};
