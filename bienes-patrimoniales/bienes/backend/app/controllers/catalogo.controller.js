const Catalogo = require('../models/catalogo.model.js');

// Create and Save a new Catalogo
exports.create = (req, res) => {
    // Create a Catalogo
    const _catalogo = new Catalogo({
        _id: req.body._id,
        descripcion: req.body.descripcion,
        estado: req.body.estado
    });
    let body = req.body;
    console.log(body);

    // Save Catalogo the database
    _catalogo.save()
    .then(catalogo => {
        res.send(catalogo);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Catalogo."
        });
    });
};

// Retrieve and return all Catalogos from the database.
exports.findAll = (req, res) => {
    Catalogo.find()
    .then(catalogos => {
        res.send(catalogos);
        console.log(catalogos)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving catalogos."
        });
    });
};

// Find a single catalogo with a catalogoId
exports.findOne = (req, res) => {
    Catalogo.findById(req.params._id)
    .then(catalogo => {
        if(!catalogo) {
            return res.status(404).send({
                message: "Catalogo not found with id " + req.params._id
            });
        }
        res.send(catalogo);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Catalogo not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error retrieving catalogo with id " + req.params._id
        });
    });
};

// Update an catalogo identified by the catalogoID in the request
exports.update = (req, res) => {
    // Find catalogo and update it with the request body
    Catalogo.findByIdAndUpdate(req.params._id, {
        descripcion: req.body.descripcion,
        estado: req.body.estado
    }, {new: true})
    .then(catalogo => {
        if(!catalogo) {
            return res.status(404).send({
                message: "Catalogo not found with id " + req.params._id
            });
        }
        res.send(catalogo);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Catalogo not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error updating catalogo with id " + req.params._id
        });
    });
};

// Delete a catalogo with the specified catalogoId in the request
exports.delete = (req, res) => {
    Catalogo.findByIdAndRemove(req.params._id)
    .then(catalogo => {
        if(!catalogo) {
            return res.status(404).send({
                message: "Catalogo not found with id " + req.params._id
            });
        }
        res.send({message: "Catalogo deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Catalogo not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Could not delete catalogo with id " + req.params._id
        });
    });
};
