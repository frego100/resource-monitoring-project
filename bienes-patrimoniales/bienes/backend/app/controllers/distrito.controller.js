const Distrito = require('../models/distrito.model.js');

// Create and Save a new Distrito
exports.create = (req, res) => {
    // Create a Distrito
    const _distrito = new Distrito({
        _id: req.body._id,
        nombre_distrito: req.body.nombre_distrito,
        descripcion: req.body.descripcion
    });
    let body = req.body;
    console.log(body);

    // Save Distrito the database
    _distrito.save()
    .then(distrito => {
        res.send(distrito);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Distrito."
        });
    });
};

// Retrieve and return all distritos from the database.
exports.findAll = (req, res) => {
    Distrito.find()
    .then(distritos => {
        res.send(distritos);
        console.log(distritos)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving distritos."
        });
    });
};

// Find a single distrito with a distritoId
exports.findOne = (req, res) => {
    Distrito.findById(req.params.id_distrito)
    .then(distrito => {
        if(!distrito) {
            return res.status(404).send({
                message: "Distrito not found with id " + req.params.id_distrito
            });
        }
        res.send(distrito);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Distrito not found with id " + req.params.id_distrito
            });
        }
        return res.status(500).send({
            message: "Error retrieving distrito with id " + req.params.id_distrito
        });
    });
};

// Update an distrito identified by the distritoId in the request
exports.update = (req, res) => {
    // Find distrito and update it with the request body
    Distrito.findByIdAndUpdate(req.params.id_distrito, {
        nombre_distrito: req.body.nombre_distrito,
        descripcion: req.body.descripcion
    }, {new: true})
    .then(distrito => {
        if(!distrito) {
            return res.status(404).send({
                message: "Distrito not found with id " + req.params.id_distrito
            });
        }
        res.send(distrito);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Distrito not found with id " + req.params.id_distrito
            });
        }
        return res.status(500).send({
            message: "Error updating distrito with id " + req.params.id_distrito
        });
    });
};

// Delete a distrito with the specified distritoId in the request
exports.delete = (req, res) => {
    Distrito.findByIdAndRemove(req.params.id_distrito)
    .then(distrito => {
        if(!distrito) {
            return res.status(404).send({
                message: "Distrito not found with id " + req.params.id_distrito
            });
        }
        res.send({message: "Distrito deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Distrito not found with id " + req.params.id_distrito
            });
        }
        return res.status(500).send({
            message: "Could not delete distrito with id " + req.params.id_distrito
        });
    });
};
