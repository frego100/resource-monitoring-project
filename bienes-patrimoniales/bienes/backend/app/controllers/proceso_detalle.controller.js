const ProcesoDetalle = require('../models/proceso.model.js');

// Create and Save a new ProcesoDetalle
exports.create = (req, res) => {
    // Create a ProcesoDetalle
    const _procesoDetalle = new ProcesoDetalle({
        _id: req.body._id,
        num_resolucion: req.body.num_resolucion,
        cod_patrimonial: req.body.cod_patrimonial
    });
    let body = req.body;
    console.log(body);

    // Save ProcesoDetalle the database
    _procesoDetalle.save()
    .then(_procesoDetalle => {
        res.send(_procesoDetalle);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the ProcesoDetalle."
        });
    });
};

// Retrieve and return all procesos_detalles from the database.
exports.findAll = (req, res) => {
    ProcesoDetalle.find()
    .then(procesosDetalle => {
        res.send(procesosDetalle);
        console.log(procesosDetalle)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving procesos_detalle."
        });
    });
};

// Find a single proceso_detalle with a proceso_detalleId
exports.findOne = (req, res) => {
    ProcesoDetalle.findById(req.params._id)
    .then(procesoDetalle => {
        if(!procesoDetalle) {
            return res.status(404).send({
                message: "Proceso_Detalle not found with id " + req.params._id
            });
        }
        res.send(procesoDetalle);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Proceso_Detalle not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error retrieving proceso_detalle with id " + req.params._id
        });
    });
};

// Update an proceso_detalle identified by the procesodetalleID in the request
exports.update = (req, res) => {
    // Find proceso_Detalle and update it with the request body
    ProcesoDetalle.findByIdAndUpdate(req.params._id, {
      num_resolucion: req.body.num_resolucion,
      cod_patrimonial: req.body.cod_patrimonial
    }, {new: true})
    .then(procesoDetalle => {
        if(!procesoDetalle) {
            return res.status(404).send({
                message: "Proceso_Detalle not found with id " + req.params._id
            });
        }
        res.send(procesoDetalle);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Proceso_Detalle not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error updating proceso_detalle with id " + req.params._id
        });
    });
};

// Delete a proceso_detalle with the specified procesoDetalleId in the request
exports.delete = (req, res) => {
    ProcesoDetalle.findByIdAndRemove(req.params._id)
    .then(procesoDetalle => {
        if(!procesoDetalle) {
            return res.status(404).send({
                message: "ProcesoDetalle not found with id " + req.params._id
            });
        }
        res.send({message: "ProcesoDetalle deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "ProcesoDetalle not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Could not delete proceso_Detalle with id " + req.params._id
        });
    });
};
