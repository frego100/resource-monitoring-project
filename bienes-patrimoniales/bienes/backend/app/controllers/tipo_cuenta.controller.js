const TipoCuenta = require('../models/tipo_cuenta.model');


exports.create = (req,res) =>{
    const tipoCuenta = new TipoCuenta({
        _id: req.body._id,
        descripcion: req.body.descripcion
    });
    let body = req.body;
    console.log(body);

    tipoCuenta.save()
    .then(tipoCuenta =>{
        res.send(tipoCuenta);
    }).catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occurred while creating the TipoCuenta."
        });
    })
};

exports.findAll = (req, res) =>{
    TipoCuenta.find()
    .then(tipoCuenta=> {
        res.send(tipoCuenta);
        console.log(tipoCuenta)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving tipoCuentas."
        });
    });
};

exports.findOne = (req, res) =>{
    TipoCuenta.findById(req.params.id_tipoCuenta)
    .then(tipoCuenta => {
        if(!tipoCuenta) {
            return res.status(404).send({
                message: "TipoCuenta not found with id " + req.params.id_tipoCuenta
            });
        }
        res.send(tipoCuenta);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "TipoCuenta not found with id " + req.params.id_tipoCuenta
            });
        }
        return res.status(500).send({
            message: "Error retrieving tipoCuenta with id " + req.params.id_tipoCuenta
        });
    });
};

exports.update = (req, res) =>{
    TipoCuenta.findByIdAndUpdate(req.params.id_tipoCuenta,{
        descripcion: req.body.descripcion
    }, {new:true})
    .then(tipoCuenta => {
        if(!tipoCuenta) {
            return res.status(404).send({
                message: "TipoCuenta not found with id " + req.params.id_tipoCuenta
            });
        }
        res.send(tipoCuenta);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "TipoCuenta not found with id " + req.params.id_tipoCuenta
            });
        }
        return res.status(500).send({
            message: "Error updating tipoCuenta with id " + req.params.id_tipoCuenta
        });
    });

};

exports.delete = (req, res) =>{
    TipoCuenta.findByIdAndRemove(req.params.id_tipoCuenta)
    .then(tipoCuenta => {
        if(!tipoCuenta) {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_tipoCuenta
            });
        }
        res.send({message: "TipoCuenta deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "TipoCuenta not found with id " + req.params.id_tipoCuenta
            });
        }
        return res.status(500).send({
            message: "Could not delete tipoCuenta with id " + req.params.id_tipoCuenta
        });
    });

};