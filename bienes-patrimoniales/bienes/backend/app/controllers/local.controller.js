const Local = require('../models/local.model');
const Departamento = require('../models/departamento.model')

exports.create = (req,res) =>{
    const local = new Local({
        id_entidad: req.body.id_entidad,
        id_direccion: req.body.id_direccion,
        nombre: req.body.nombre,
        propiedad: req.body.propiedad,
        unidad_medida: req.body.unidad_medida,
        id_departamento: req.body.id_departamento,
        id_provincia: req.body.id_provincia,
        id_distrito: req.body.id_distrito,
        id_tipoCuenta: req.body.id_tipoCuenta,
        id_moneda: req.body.id_moneda,
        cuenta: req.body.cuenta,
        valor_contable: req.body.valor_contable,
        oficina_registral: req.body.oficina_registral,
        tomo: req.body.tomo,
        fojas: req.body.fojas,
        asiento: req.body.asiento,
        cod_predio: req.body.cod_predio,
        partida_electronica: req.body.partida_electronica,
        partida_sinabif: req.body.partida_sinabif,
        prop_registral: req.body.prop_registral,
        beneficiario: req.body.beneficiario,
        estadoActivo: true
    });
    let body = req.body;
    console.log(body);

    local.save()
    .then(local =>{
        res.send(local);
    }).catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occurred while creating the locals."
        });
    })
};

exports.findAll = (req, res) =>{
    /*Local.find({}, function(err, local){
        Departamento.populate(local, {path: "id_departamento"}, function(err, local){
            res.status(200).send(local);
        });

    });*/
    Local.find()
    .then(local=> {
        res.send(local);
        console.log(local)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving locals."
        });
    });
};

exports.findOne = (req, res) =>{
    Local.findById(req.params.id_local)
    .then(local => {
        if(!local) {
            return res.status(404).send({
                message: "Local not found with id " + req.params.id_local
            });
        }
        res.send(local);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Local not found with id " + req.params.id_local
            });
        }
        return res.status(500).send({
            message: "Error retrieving local with id " + req.params.id_local
        });
    });
};

exports.update = (req, res) =>{
    Local.findByIdAndUpdate(req.params.id_local,{
        id_entidad: req.body.id_entidad,
        id_direccion: req.body.id_direccion,
        nombre: req.body.nombre,
        propiedad: req.body.propiedad,
        unidad_medida: req.body.unidad_medida,
        id_departamento: req.body.id_departamento,
        id_provincia: req.body.id_provincia,
        id_distrito: req.body.id_distrito,
        id_tipoCuenta: req.body.id_tipoCuenta,
        id_moneda: req.body.id_moneda,
        cuenta: req.body.cuenta,
        valor_contable: req.body.valor_contable,
        oficina_registral: req.body.oficina_registral,
        tomo: req.body.tomo,
        fojas: req.body.fojas,
        asiento: req.body.asiento,
        cod_predio: req.body.cod_predio,
        partida_electronica: req.body.partida_electronica,
        partida_sinabif: req.body.partida_sinabif,
        prop_registral: req.body.prop_registral,
        beneficiario: req.body.beneficiario,
        estadoActivo: true
    }, {new:true})
    .then(local => {
        if(!local) {
            return res.status(404).send({
                message: "Local not found with id " + req.params.id_local
            });
        }
        res.send(local);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Local not found with id " + req.params.id_local
            });
        }
        return res.status(500).send({
            message: "Error updating local with id " + req.params.id_local
        });
    });

};

exports.delete = (req, res) =>{
    Local.findByIdAndRemove(req.params.id_local)
    .then(local => {
        if(!local) {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_local
            });
        }
        res.send({message: "Local deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Local not found with id " + req.params.id_local
            });
        }
        return res.status(500).send({
            message: "Could not delete local with id " + req.params.id_local
        });
    });

};