const Direccion = require('../models/direccion.model');

exports.create = (req,res) =>{
    const direccion = new Direccion({
        _id: req.body._id,
        descripcion: req.body.descripcion,
        numero: req.body.numero,
        manzana:  req.body.manzana,
        lote: req.body.lote
    });
    let body = req.body;
    console.log(body);

    direccion.save()
    .then(direccion =>{
        res.send(direccion);
    }).catch(err =>{
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Direccion."
        });
    })
};

exports.findAll = (req, res) =>{
    Direccion.find()
    .then(direccion=> {
        res.send(direccion);
        console.log(direccion)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving direccions."
        });
    });
};

exports.findOne = (req, res) =>{
    Direccion.findById(req.params.id_direccion)
    .then(direccion => {
        if(!direccion) {
            return res.status(404).send({
                message: "Direccion not found with id " + req.params.id_direccion
            });
        }
        res.send(direccion);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Direccion not found with id " + req.params.id_direccion
            });
        }
        return res.status(500).send({
            message: "Error retrieving direccion with id " + req.params.id_direccion
        });
    });
};

exports.update = (req, res) =>{
    Direccion.findByIdAndUpdate(req.params.id_direccion,{
        descripcion: req.body.descripcion,
        numero: req.body.numero,
        manzana:  req.body.manzana,
        lote: req.body.lote
    }, {new:true})
    .then(direccion => {
        if(!direccion) {
            return res.status(404).send({
                message: "Direccion not found with id " + req.params.id_direccion
            });
        }
        res.send(direccion);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Direccion not found with id " + req.params.id_direccion
            });
        }
        return res.status(500).send({
            message: "Error updating direccion with id " + req.params.id_direccion
        });
    });

};

exports.delete = (req, res) =>{
    Direccion.findByIdAndRemove(req.params.id_direccion)
    .then(direccion => {
        if(!direccion) {
            return res.status(404).send({
                message: "Department not found with id " + req.params.id_direccion
            });
        }
        res.send({message: "Direccion deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Direccion not found with id " + req.params.id_direccion
            });
        }
        return res.status(500).send({
            message: "Could not delete direccion with id " + req.params.id_direccion
        });
    });

};