const Moneda = require('../models/moneda.model.js');

// Create and Save a new Moneda
exports.create = (req, res) => {
    // Create a Moneda
    const moneda = new Moneda({
        _id: req.body._id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion
    });
    let body = req.body;
    console.log(body);

    // Save Moneda the database
    moneda.save()
    .then(moneda => {
        res.send(moneda);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Moneda."
        });
    });
};

// Retrieve and return all monedas from the database.
exports.findAll = (req, res) => {
    Moneda.find()
    .then(monedas => {
        res.send(monedas);
        console.log(monedas)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving monedas."
        });
    });
};

// Find a single moneda with a monedaId
exports.findOne = (req, res) => {
    Moneda.findById(req.params.id_moneda)
    .then(moneda => {
        if(!moneda) {
            return res.status(404).send({
                message: "Moneda not found with id " + req.params.id_moneda
            });
        }
        res.send(moneda);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Moneda not found with id " + req.params.id_moneda
            });
        }
        return res.status(500).send({
            message: "Error retrieving moneda with id " + req.params.id_moneda
        });
    });
};

// Update an moneda identified by the monedaId in the request
exports.update = (req, res) => {
    // Find moneda and update it with the request body
    Moneda.findByIdAndUpdate(req.params.id_moneda, {
        nombre_moneda: req.body.nombre_moneda,
        descripcion: req.body.descripcion
    }, {new: true})
    .then(moneda => {
        if(!moneda) {
            return res.status(404).send({
                message: "Moneda not found with id " + req.params.id_moneda
            });
        }
        res.send(moneda);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Moneda not found with id " + req.params.id_moneda
            });
        }
        return res.status(500).send({
            message: "Error updating moneda with id " + req.params.id_moneda
        });
    });
};

// Delete a moneda with the specified monedaId in the request
exports.delete = (req, res) => {
    Moneda.findByIdAndRemove(req.params.id_moneda)
    .then(moneda => {
        if(!moneda) {
            return res.status(404).send({
                message: "Moneda not found with id " + req.params.id_moneda
            });
        }
        res.send({message: "Moneda deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Moneda not found with id " + req.params.id_moneda
            });
        }
        return res.status(500).send({
            message: "Could not delete moneda with id " + req.params.id_moneda
        });
    });
};
