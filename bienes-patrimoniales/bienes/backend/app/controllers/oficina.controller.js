const Oficina = require('../models/oficina.model');
const { Schema } = require('mongoose');

// Create and Save a new office
exports.create = (req, res) => {
    // Create a office
    const oficina = new Oficina({
        id_area: req.body.id_area,
        nombre_oficina: req.body.nombre_oficina,
        estadoActivo: true
    });
    let body = req.body;
    console.log(body);

    // Save office in the database
    oficina.save()
    .then(oficina => {
        res.send(oficina);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the office."
        });
    });
};

// Retrieve and return all offices from the database.
exports.findAll = (req, res) => {
    Oficina.find()
    .then(oficina=> {
        res.send(oficina);
        console.log(oficina)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving office."
        });
    });
};

// Find a single department with a departmentId
exports.findOne = (req, res) => {
    Oficina.findById(req.params.id_oficina)
    .then(oficina => {
        if(!oficina) {
            return res.status(404).send({
                message: "Oficce not found with id " + req.params.id_oficina
            });
        }
        res.send(oficina);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Oficce not found with id " + req.params.id_oficina
            });
        }
        return res.status(500).send({
            message: "Error retrieving office with id " + req.params.id_oficina
        });
    });
};

// Update an department identified by the departmentId in the request
exports.update = (req, res) => {
    // Find department and update it with the request body
    Oficina.findByIdAndUpdate(req.params.id_oficina, {
        id_area: req.body.id_area,
        nombre_oficina: req.body.nombre_oficina,
        estadoActivo: true
    }, {new: true})
    .then(oficina => {
        if(!oficina) {
            return res.status(404).send({
                message: "Office not found with id " + req.params.id_oficina
            });
        }
        res.send(oficina);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Office not found with id " + req.params.id_oficina
            });
        }
        return res.status(500).send({
            message: "Error updating office with id " + req.params.id_oficina
        });
    });
};

// Delete a department with the specified departmentId in the request
exports.delete = (req, res) => {
    Oficina.findByIdAndRemove(req.params.id_oficina)
    .then(oficina => {
        if(!oficina) {
            return res.status(404).send({
                message: "Office not found with id " + req.params.id_oficina
            });
        }
        res.send({message: "Office deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Office not found with id " + req.params.id_oficina
            });
        }
        return res.status(500).send({
            message: "Could not delete office with id " + req.params.id_oficina
        });
    });
};