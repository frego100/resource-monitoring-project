const Modalidad = require('../models/modalidad.model.js');

// Create and Save a new Modalidad
exports.create = (req, res) => {
    // Create a Modalidad
    const _modalidad = new Modalidad({
        _id: req.body._id,
        nombre: req.body.nombre,
        descripcion: req.body.descripcion
    });
    let body = req.body;
    console.log(body);

    // Save Modalidad the database
    _modalidad.save()
    .then(_modalidad => {
        res.send(_modalidad);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Modalidad."
        });
    });
};

// Retrieve and return all modalidades from the database.
exports.findAll = (req, res) => {
    Modalidad.find()
    .then(modalidades => {
        res.send(modalidades);
        console.log(modalidades)
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving modalidades."
        });
    });
};

// Find a single modalidad with a modalidadId
exports.findOne = (req, res) => {
    Modalidad.findById(req.params._id)
    .then(modalidad => {
        if(!modalidad) {
            return res.status(404).send({
                message: "Modalidad not found with id " + req.params._id
            });
        }
        res.send(modalidad);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Modalidad not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error retrieving Modalidad with id " + req.params._id
        });
    });
};

// Update an modalidad identified by the modalidadID in the request
exports.update = (req, res) => {
    // Find modalidad and update it with the request body
    Modalidad.findByIdAndUpdate(req.params._id, {
        nombre: req.body.nombre,
        descripcion: req.body.descripcion
    }, {new: true})
    .then(modalidad => {
        if(!modalidad) {
            return res.status(404).send({
                message: "Modalidad not found with id " + req.params._id
            });
        }
        res.send(modalidad);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Modalidad not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Error updating Modalidad with id " + req.params._id
        });
    });
};

// Delete a Modalidad with the specified modalidadId in the request
exports.delete = (req, res) => {
    Modalidad.findByIdAndRemove(req.params._id)
    .then(modalidad => {
        if(!modalidad) {
            return res.status(404).send({
                message: "modalidad not found with id " + req.params._id
            });
        }
        res.send({message: "modalidad deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "modalidad not found with id " + req.params._id
            });
        }
        return res.status(500).send({
            message: "Could not delete modalidad with id " + req.params._id
        });
    });
};
