const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const AreaSchema = mongoose.Schema({
    _id: String,
    id_local: {type: String, ref: "LOCAL"},
    nombre_area: String,
    siglas_area: String
});
module.exports = mongoose.model('AREA', AreaSchema);