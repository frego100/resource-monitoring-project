const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const OficinaSchema = mongoose.Schema({
    id_area: {type: String, ref: "AREA"},
    nombre_oficina: String
});
module.exports = mongoose.model('OFICINA',OficinaSchema);