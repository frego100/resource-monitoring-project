const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const DepartamentoSchema = mongoose.Schema({
    _id: String,
    nombre: String,
    descripcion: String,
    estadoActivo: Boolean
});

module.exports = mongoose.model('DEPARTAMENTO', DepartamentoSchema);
