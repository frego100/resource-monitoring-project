const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const TipoCuentaSchema = mongoose.Schema({
    _id: String,
    descripcion: String
});
module.exports = mongoose.model('TIPO_CUENTA', TipoCuentaSchema);