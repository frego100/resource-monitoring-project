const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const DistritoSchema = mongoose.Schema({
    _id: String,
    nombre_distrito: String,
    descripcion: String
});

module.exports = mongoose.model('DISTRITO', DistritoSchema);
