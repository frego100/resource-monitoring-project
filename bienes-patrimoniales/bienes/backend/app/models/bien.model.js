const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const BienSchema = mongoose.Schema({
    _id: String, //código patrimonial
    cod_interno: Number,
    id_catalogo: {type: String, ref: "CATALOGO"},
    denominacion: String,
    resulocion_alta: String,
    marca: String,
    modelo: String,
    tipo: String,
    serie: String,
    dimensiones: String,
    estado: String,
    cuenta_contable: String,
    valor_neto: String,
    causal_alta: String,
    causal_baja: String,
    acto_disposicion: String,
    valor_tasacion: String,
    ubicacion: String,
    tiempo_permanencia: String
});

module.exports = mongoose.model('BIEN', BienSchema);
