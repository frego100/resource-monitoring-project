const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const ProvinciaSchema = mongoose.Schema({
    _id: String,
    nombre_provincia: String,
    descripcion: String
});

module.exports = mongoose.model('PROVINCIA', ProvinciaSchema);
