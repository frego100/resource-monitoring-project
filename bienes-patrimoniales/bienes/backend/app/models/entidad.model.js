const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const EntidadSchema = mongoose.Schema({
    _id: String,
    nombre_entidad: String,
    dependencia: String
});

module.exports = mongoose.model('ENTIDAD', EntidadSchema);
