const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const UsuarioSchema = mongoose.Schema({
    _id: String,  //doc_usuario
    id_tipo_doc_identidad: {type: String, ref: "TIPO_DOC_IDENTIDAD"},
    id_area: {type: String, ref: "AREA"},
    id_oficina: {type: String, ref: "OFICINA"},
    id_local: {type: String, ref: "LOCAL"},
    id_rol_usuario: {type: String, ref: "ROL_USUARIO"},
    nombre: String,
    email: String,
    avatar_url:String,
    modalidad: String,
    estadoActivo: Boolean,
    id_ultima_sesion: { type: Schema.ObjectId, ref: "ACTIVIDAD_SESION" }
});
module.exports = mongoose.model('USUARIO', UsuarioSchema);  