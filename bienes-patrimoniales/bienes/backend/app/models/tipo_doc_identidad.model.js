const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const TipoDocIdentidad = mongoose.Schema({
    _id:String,
    descripcion: String,
    estado: String,
    estadoActivo: Boolean
});
module.exports = mongoose.model('TIPO_DOC_IDENTIDAD',TipoDocIdentidad);