const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const DireccionSchema = mongoose.Schema({
    _id: String,
    descripcion: String,
    numero: Number,
    manzana:  String,
    lote: String
});
module.exports = mongoose.model('DIRECCION', DireccionSchema);