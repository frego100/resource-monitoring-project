const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const CatalogoSchema = mongoose.Schema({
    _id: String,
    descripcion: String,
    estado: String
});

module.exports = mongoose.model('CATALOGO', CatalogoSchema);
