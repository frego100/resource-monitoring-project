const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const TipoProcesoSchema = mongoose.Schema({
    _id: String,
    id_modalidad: {type: String, ref: "MODALIDAD"},
    nombre: String,
    descripcion: String
});

module.exports = mongoose.model('TIPO_PROCESO', TipoProcesoSchema);
