const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const ProcesoSchema = mongoose.Schema({
    _id: String,//num_resolucion
    tipo_proceso: { type: String, ref: "TIPO_PROCESO"},
    id_actividad_sesion: { type: String, ref: "ACTIVIDAD_SESION"}
});

module.exports = mongoose.model('PROCESO', ProcesoSchema);
