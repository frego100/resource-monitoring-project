const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const MonedaSchema = mongoose.Schema({
    _id: String,
    nombre: String,
    descripcion: String,
});

module.exports = mongoose.model('MONEDA', MonedaSchema);