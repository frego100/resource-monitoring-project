const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const LocalSchema = mongoose.Schema({
    id_entidad: {type: String, ref: "ENTIDAD"},
    id_direccion: {type: String, ref: "DIRECCION"},
    nombre: String,
    propiedad: String,
    unidad_medida: String,
    id_departamento: {type: String, ref: "DEPARTAMENTO"},
    id_provincia: {type: String, ref: "PROVINCIA"},
    id_distrito: {type: String, ref: "DISTRITO"},
    id_tipoCuenta: {type: String, ref: "TIPO_CUENTA"},
    id_moneda: {type: String, ref: "MONEDA"},
    cuenta: String,
    valor_contable: String,
    oficina_registral: String,
    tomo: String,
    fojas: String,
    asiento: String,
    cod_predio: String,
    partida_electronica: String,
    partida_sinabif: String,
    prop_registral: String,
    beneficiario: String 
});
module.exports = mongoose.model('LOCAL', LocalSchema);