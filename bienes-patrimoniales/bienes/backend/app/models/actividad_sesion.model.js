const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const ActividadSesionSchema = mongoose.Schema({
    //_id: String,
    id_usuario: {type: String, ref: "USUARIO"},
    fecha_inicio: String,
    fecha_fin: String,
    hora_inicio: String,
    hora_fin: String,
    estado: String
});
module.exports = mongoose.model('ACTIVIDAD_SESION', ActividadSesionSchema);
