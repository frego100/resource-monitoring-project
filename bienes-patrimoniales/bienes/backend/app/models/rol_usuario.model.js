const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const RolUsuario = mongoose.Schema({
    _id:String,
    descripcion: String,
    estado: String,
    estadoActivo: Boolean
});
module.exports = mongoose.model('ROL_USUARIO',RolUsuario);