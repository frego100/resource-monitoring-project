const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const ModalidadSchema = mongoose.Schema({
    _id: String,
    nombre: String,
    descripcion: String
});

module.exports = mongoose.model('MODALIDAD', ModalidadSchema);
