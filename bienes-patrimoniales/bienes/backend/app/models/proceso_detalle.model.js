const mongoose = require('mongoose');
var Schema = mongoose.Schema;

const ProcesoDetalleSchema = mongoose.Schema({
    _id: String,
    num_resolucion: {type: String, ref: "PROCESO"},
    cod_patrimonial: {type: String, ref: "BIEN"}
});

module.exports = mongoose.model('PROCESO_DETALLE', ProcesoDetalleSchema);
