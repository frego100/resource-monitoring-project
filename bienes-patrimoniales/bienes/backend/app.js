var express = require('express');
var logger = require('morgan');
const keys = require('./config/keys');
const passport = require('passport');
const cors = require("cors");
const cookieSesion = require('cookie-session');


var app = express();

app.use(cookieSesion({
  maxAge: 24*60*60*1000,
  keys: [keys.session.cookieKey]
}));

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(
  cors({
    origin: "http://localhost:3000", // allow to server to accept request from different origin
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true // allow session cookie from browser to pass through
  })
);

//Rutas
var departamentos = require('./routes/departamento');
var locales = require('./routes/local');
var usuarios = require('./routes/usuario');
var bienes = require('./routes/bien');
var entidades = require('./routes/entidad');
var provincias = require('./routes/provincia');
var distritos = require('./routes/distrito');
var catalogos = require('./routes/catalogo');
var procesos = require('./routes/proceso');
var procesosDetalle = require('./routes/proceso_detalle');
var modalidades = require('./routes/modalidad');
var tiposProceso = require('./routes/tipo_proceso');
var monedas = require('./routes/moneda');
var tiposCuenta = require('./routes/tipo_cuenta');
var actividadesSesion = require('./routes/actividad_sesion');
var areas = require('./routes/area');
var direcciones = require('./routes/direccion');
var oficina = require('./routes/oficina');
var tipo_doc_identidad = require('./routes/tipo_doc_identidad');
var rol_usuario = require('./routes/rol_usuario');

app.use('/api/departamentos', departamentos);
app.use('/api/locales', locales);
app.use('/api/bienes', bienes);
app.use('/api/entidades', entidades);
app.use('/api/provincias', provincias);
app.use('/api/distritos', distritos);
app.use('/api/catalogos', catalogos);
app.use('/api/procesos', procesos);
app.use('/api/procesosDetalle', procesosDetalle);
app.use('/api/modalidades', modalidades);
app.use('/api/tiposProceso', tiposProceso);
app.use('/api/usuarios', usuarios);
app.use('/api/monedas', monedas);
app.use('/api/tiposCuenta', tiposCuenta);
app.use('/api/actividadesSesion', actividadesSesion);
app.use('/api/areas', areas);
app.use('/api/direcciones', direcciones);
app.use('/api/oficinas', oficina);
app.use('/api/tipo_doc_identidad', tipo_doc_identidad);
app.use('/api/rol_usuario', rol_usuario);

// Google oauth

const auth = require('./routes/oauth');
app.use(passport.initialize())
app.use(passport.session())
app.use('/auth', auth);

const authCheck = (req, res, next) => {
  if (!req.user) {
    res.status(401).json({
      authenticated: false,
      message: "user has not been authenticated"
    });
  } else {
    next();
  }
};

app.get("/", authCheck, (req, res) => {
  res.status(200).json({
    authenticated: true,
    message: "user succefull autenticated",
    user: req.user,
    cookies: req.cookies
  });
})

module.exports = app;
